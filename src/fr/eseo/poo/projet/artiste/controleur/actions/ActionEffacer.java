package fr.eseo.poo.projet.artiste.controleur.actions;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;

public class ActionEffacer extends AbstractAction {

    /**
     * <h2>Constante de classe</h2>
     * Nom affiché sur le composant réalisant l'action.
     */
    public static final String NOM_ACTION = "Tout effacer";

    private PanneauDessin panel;

    /**
     * <h2>Constructeur par défaut</h2>
     *
     * @param panel {@link PanneauDessin PanneauDessin} à effacer.
     */
    public ActionEffacer(PanneauDessin panel) {
        super(NOM_ACTION);
        this.panel = panel;
    }

    public PanneauDessin getPanel() {
        return panel;
    }

    private void setPanel(PanneauDessin panel) {
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        int n = JOptionPane.showConfirmDialog(panel, "Voulez-vous tout effacer ?",
                NOM_ACTION, JOptionPane.YES_NO_OPTION);
        // 0 if user chose "yes"
        if (n == 0) {
            panel.getVueFormes().clear();
            panel.repaint();
            System.out.println("Clear all");
        }
    }
}
