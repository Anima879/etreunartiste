package fr.eseo.poo.projet.artiste.controleur.actions;

import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;
import fr.eseo.poo.projet.artiste.xml.LecteurSVG;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.xml.xpath.XPathExpressionException;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public class ActionOuvrir extends AbstractAction {

    public static final String NOM_ACTION = "Ouvrir";

    private final PanneauDessin panel;

    public ActionOuvrir(PanneauDessin panel) {
        this.panel = panel;
    }

    public PanneauDessin getPanel() {
        return panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        LecteurSVG lecteurSVG = new LecteurSVG();
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("C:\\Users\\eloim\\Documents\\ESEO\\I1\\Grand Projet " +
                "Informatique\\EtreUnArtiste\\dessins"));
        int value = chooser.showOpenDialog(panel);
        if (value == JFileChooser.APPROVE_OPTION) {
            String file = chooser.getSelectedFile().toString();
            try {
                List<VueForme> dessins = lecteurSVG.lisDessin(file);
                panel.getVueFormes().removeAll(panel.getVueFormes());
                for (VueForme vue : dessins) {
                    panel.ajouterVueForme(vue);
                }
                panel.repaint();
                System.out.println("Load file : " + file);
            } catch (XPathExpressionException xPathExpressionException) {
                xPathExpressionException.printStackTrace();
            }
        }
    }
}
