package fr.eseo.poo.projet.artiste.controleur.actions;

import fr.eseo.poo.projet.artiste.controleur.outils.OutilCercle;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilEllipse;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilEtoile;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilLigne;
import fr.eseo.poo.projet.artiste.controleur.outils.OutilRectangle;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;

public class ActionChoisirForme extends AbstractAction {

    public static final String NOM_ACTION_LIGNE = "Ligne";
    public static final String NOM_ACTION_ELLIPSE = "Ellipse";
    public static final String NOM_ACTION_CERCLE = "Cercle";
    public static final String NOM_ACTION_ETOILE = "Etoile";
    public static final String NOM_ACTION_RECTANGLE = "Rectangle";

    private PanneauDessin panel;
    private PanneauBarreOutils toolPanel;
    private String action;

    public ActionChoisirForme(PanneauDessin panel, PanneauBarreOutils toolPanel, String action) {
        this.panel = panel;
        this.toolPanel = toolPanel;
        this.action = action;
    }

    public PanneauDessin getPanel() {
        return panel;
    }

    public void setPanel(PanneauDessin panel) {
        this.panel = panel;
    }

    public PanneauBarreOutils getToolPanel() {
        return toolPanel;
    }

    public void setToolPanel(PanneauBarreOutils toolPanel) {
        this.toolPanel = toolPanel;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (this.action) {
            case NOM_ACTION_LIGNE:
                System.out.println("User choose Ligne");
                this.getPanel().associerOutil(new OutilLigne());
                break;
            case NOM_ACTION_ELLIPSE:
                System.out.println("User choose Ellipse");
                this.getPanel().associerOutil(new OutilEllipse());
                break;
            case NOM_ACTION_CERCLE:
                System.out.println("User choose Cercle");
                this.getPanel().associerOutil(new OutilCercle());
                break;
            case NOM_ACTION_ETOILE:
                System.out.println("User choose Etoile");
                this.getPanel().associerOutil(new OutilEtoile(toolPanel));
                break;
            case NOM_ACTION_RECTANGLE:
                System.out.println("User choose Rectangle");
                this.getPanel().associerOutil(new OutilRectangle());
                break;
            default:
                throw new IllegalArgumentException();
        }
    }
}
