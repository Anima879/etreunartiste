package fr.eseo.poo.projet.artiste.controleur.actions;

import fr.eseo.poo.projet.artiste.controleur.outils.OutilEffacer;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;

public class ActionSimpleEffacer extends AbstractAction {

    public static final String NOM_ACTION = "Effacer";

    private PanneauDessin panel;

    /**
     * <h2>Constructeur par défaut</h2>
     *
     * @param panel {@link PanneauDessin PanneauDessin} contenant les formes pouvant
     *                                                 être sélectionner.
     */
    public ActionSimpleEffacer(PanneauDessin panel) {
        this.panel = panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.panel.associerOutil(new OutilEffacer());
        System.out.println("User choose " + NOM_ACTION);
    }

    public PanneauDessin getPanel() {
        return panel;
    }

    public void setPanel(PanneauDessin panel) {
        this.panel = panel;
    }
}
