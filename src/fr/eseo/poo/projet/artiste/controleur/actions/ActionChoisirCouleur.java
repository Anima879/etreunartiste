package fr.eseo.poo.projet.artiste.controleur.actions;

import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.AbstractAction;
import javax.swing.JColorChooser;
import java.awt.Color;
import java.awt.event.ActionEvent;

public class ActionChoisirCouleur extends AbstractAction {

    public static final String NOM_ACTION = "Choisir couleur";

    private final PanneauDessin panel;
    private final Color couleur;

    public ActionChoisirCouleur(PanneauDessin panel) {
        this.panel = panel;
        this.couleur = Forme.COULEUR_PAR_DEFAUT;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(panel, NOM_ACTION, this.couleur);
        if (c != null) {
            System.out.println("Color set to " + c);
            panel.setCouleurCourante(c);
        }
    }
}
