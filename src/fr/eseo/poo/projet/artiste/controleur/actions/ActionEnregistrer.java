package fr.eseo.poo.projet.artiste.controleur.actions;

import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;
import fr.eseo.poo.projet.artiste.xml.EnregistreurSVG;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;

public class ActionEnregistrer extends AbstractAction {
    public static final String NOM_ACTION = "Enregistrer";

    private final PanneauDessin panel;

    public ActionEnregistrer(PanneauDessin panel) {
        this.panel = panel;
    }

    public PanneauDessin getPanel() {
        return panel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        EnregistreurSVG enregistreurSVG = new EnregistreurSVG();
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File("C:\\Users\\eloim\\Documents\\ESEO\\I1\\Grand Projet " +
                "Informatique\\EtreUnArtiste\\dessins"));
        int value = chooser.showSaveDialog(panel);
        if (value == JFileChooser.APPROVE_OPTION) {
            String file = chooser.getSelectedFile().toString();
            if (!file.endsWith(".svg"))
                file += ".svg";

            try {
                enregistreurSVG.enregistreDessin(file, panel.getVueFormes());
                System.out.println("Save file : " + file);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}
