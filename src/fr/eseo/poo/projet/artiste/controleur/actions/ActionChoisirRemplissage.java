package fr.eseo.poo.projet.artiste.controleur.actions;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;

/**
 * Classe utiliser pour gérer l'action de remplissage des {@link fr.eseo.poo.projet.artiste.modele.formes.Forme formes}
 * pouvant être remplies.
 * L'action est liée à un et un seul {@link PanneauDessin PanneauDessin}
 * @see AbstractAction
 * @see PanneauDessin
 */
public class ActionChoisirRemplissage extends AbstractAction {

    public static final String NOM_ACTION = "Remplissage";

    private final PanneauDessin panel;

    public ActionChoisirRemplissage(PanneauDessin panel) {
        this.panel = panel;
    }

    /**
     * Fait basculer le mode de remplissage du {@link PanneauDessin PanneauDessin} à chaque appel.
     * @param e A semantic event which indicates that a component-defined action occurred.
     * @see ActionEvent
     * @see AbstractAction
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        panel.setModeRemplissage(!panel.getModeRemplissage());
        System.out.println("Fill is " + panel.getModeRemplissage());
    }
}
