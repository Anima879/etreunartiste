package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Rectangle;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.formes.VueRectangle;

public class OutilRectangle extends OutilForme {

    public OutilRectangle() {
    }

    @Override
    protected VueForme creerVueForme() {
        double largeur = Math.abs(this.getFin().getAbscisse() - this.getDebut().getAbscisse());
        double hauteur = Math.abs(this.getFin().getOrdonnee() - this.getDebut().getOrdonnee());
        double x = Math.min(this.getDebut().getAbscisse(), this.getFin().getAbscisse());
        double y = Math.min(this.getDebut().getOrdonnee(), this.getFin().getOrdonnee());

        Rectangle rect = new Rectangle(new Coordonnees(x, y), largeur, hauteur);
        rect.setCouleur(this.getPanneauDessin().getCouleurCourante());
        rect.setRempli(this.getPanneauDessin().getModeRemplissage());
        System.out.println(rect);

        return new VueRectangle(rect);
    }
}
