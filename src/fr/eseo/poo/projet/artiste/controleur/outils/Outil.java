package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.formes.VueRectangle;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.event.MouseInputListener;
import java.awt.event.MouseEvent;


/**
 * <h1>Classe abstraite Outil</h1>
 * Classe abstraite commun à toutes les classes voulant contrôler les classes
 * VueForme. Elle implémente l'interface {@link MouseInputListener MouseInputListetner} pour écouter
 * les événements graphiques de la souris de l'utilisateur.
 * Chaque outil doit être lié à un et un seul PanneauDessin et réciproquement.
 * @see fr.eseo.poo.projet.artiste.vue.formes.VueForme
 * @see PanneauDessin
 */
public abstract class Outil implements MouseInputListener {

    private Coordonnees debut;
    private Coordonnees fin;
    private PanneauDessin panneauDessin;

    public void setDebut(Coordonnees debut) {
        this.debut = debut;
    }

    public void setFin(Coordonnees fin) {
        this.fin = fin;
    }

    public PanneauDessin getPanneauDessin() {
        return panneauDessin;
    }

    public void setPanneauDessin(PanneauDessin panneauDessin) {
        this.panneauDessin = panneauDessin;
    }

    public Coordonnees getDebut() {
        return debut;
    }

    public Coordonnees getFin() {
        return fin;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        System.out.println("Mouse clicked at [" + e.getX() + ", " + e.getY() + "] " + e.getClickCount() + " times");
        this.clearSelectionRect();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        System.out.println("Mouse pressed at [" + e.getX() + ", " + e.getY() + "]");
        this.clearSelectionRect();
        this.debut = new Coordonnees(e.getX(), e.getY());
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        System.out.println("Mouse released at [" + e.getX() + ", " + e.getY() + "]");
        this.fin = new Coordonnees(e.getX(), e.getY());
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        System.out.println("Mouse dragged at [" + e.getX() + ", " + e.getY() + "]");
        this.fin = new Coordonnees(e.getX(), e.getY());
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    public void clearSelectionRect() {
        for (int i = 0; i < this.getPanneauDessin().getVueFormes().size(); i++) {
            VueForme vue = this.getPanneauDessin().getVueFormes().get(i);
            if (!(vue instanceof VueRectangle))
                continue;
            if (((VueRectangle) vue).isFromSelection())
                this.getPanneauDessin().getVueFormes().remove(i);
                this.getPanneauDessin().repaint();
            break;
        }
    }
}
