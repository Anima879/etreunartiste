package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.vue.formes.VueEllipse;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

import java.awt.event.MouseEvent;

/**
 * <p>
 *     Classe gérant la construction et l'affiche d'une ellipse.
 * </p>
 * @see VueForme
 * @see VueEllipse
 * @see Ellipse
 */
public class OutilEllipse extends OutilForme {

    public OutilEllipse() {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
    }

    @Override
    protected VueForme creerVueForme() {
        double largeur = Math.abs(this.getFin().getAbscisse() - this.getDebut().getAbscisse());
        double hauteur = Math.abs(this.getFin().getOrdonnee() - this.getDebut().getOrdonnee());
        double x = Math.min(this.getDebut().getAbscisse(), this.getFin().getAbscisse());
        double y = Math.min(this.getDebut().getOrdonnee(), this.getFin().getOrdonnee());
        Ellipse e = new Ellipse(new Coordonnees(x, y), largeur, hauteur);
        e.setCouleur(this.getPanneauDessin().getCouleurCourante());
        e.setRempli(this.getPanneauDessin().getModeRemplissage());
        System.out.println(e);
        return new VueEllipse(e);
    }
}
