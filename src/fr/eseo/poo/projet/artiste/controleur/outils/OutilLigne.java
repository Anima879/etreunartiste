package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.formes.VueLigne;

/**
 * Classe gérant le contrôle de {@link VueForme VueForme} et
 * {@link fr.eseo.poo.projet.artiste.modele.formes.Forme Forme}
 * pour la classe {@link fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin PanneauDessin} qui se charge de l'affichage.
 * @see VueForme
 * @see VueLigne
 * @see fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin
 */
public class OutilLigne extends OutilForme {

    public OutilLigne() {
    }

    @Override
    protected VueForme creerVueForme() {
        double largeur = this.getFin().getAbscisse() - this.getDebut().getAbscisse();
        double hauteur = this.getFin().getOrdonnee() - this.getDebut().getOrdonnee();
        Ligne ligne = new Ligne(this.getDebut(), largeur, hauteur);
        ligne.setCouleur(this.getPanneauDessin().getCouleurCourante());
        System.out.println(ligne);
        return new VueLigne(ligne);
    }
}
