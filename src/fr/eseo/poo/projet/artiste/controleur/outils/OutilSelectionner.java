package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.controleur.actions.ActionSelectionner;
import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.modele.formes.Rectangle;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.formes.VueRectangle;

import javax.swing.JOptionPane;
import java.awt.Color;
import java.awt.event.MouseEvent;

public class OutilSelectionner extends Outil {

    private Forme formeSelectionnee;

    public OutilSelectionner() {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        for (int i = this.getPanneauDessin().getVueFormes().size() - 1; i >= 0; i--) {
            VueForme v = this.getPanneauDessin().getVueFormes().get(i);
            if (v.getForme().contient(new Coordonnees(e.getX(), e.getY()))) {
                formeSelectionnee = v.getForme();
                System.out.println("Selected Forme : " + formeSelectionnee.toString());

                Coordonnees point = new Coordonnees(formeSelectionnee.getCadreMinX(), formeSelectionnee.getCadreMinY());
                Rectangle rect = new Rectangle(point, Math.abs(formeSelectionnee.getLargeur()),
                        Math.abs(formeSelectionnee.getHauteur()));
                rect.setCouleur(Color.gray);
                VueRectangle vueRect = new VueRectangle(rect);
                vueRect.setFromSelection(true);
                this.getPanneauDessin().ajouterVueForme(vueRect);
                this.getPanneauDessin().repaint();

                JOptionPane.showMessageDialog(this.getPanneauDessin(), v.getForme().toString(),
                        ActionSelectionner.NOM_ACTION, JOptionPane.INFORMATION_MESSAGE);
                break;
            }
        }
    }



    public Forme getFormeSelectionnee() {
        return formeSelectionnee;
    }
}
