package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.controleur.actions.ActionSelectionner;
import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.JOptionPane;
import java.awt.event.MouseEvent;

public class OutilEffacer extends Outil {

    public OutilEffacer() {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        for (int i = this.getPanneauDessin().getVueFormes().size() - 1; i >= 0; i--) {
            VueForme v = this.getPanneauDessin().getVueFormes().get(i);
            if (v.getForme().contient(new Coordonnees(e.getX(), e.getY()))) {
                System.out.println("Erase Forme : " + v.getForme().toString());
                this.getPanneauDessin().getVueFormes().remove(i);
                this.getPanneauDessin().repaint();
                break;
            }
        }
    }

}
