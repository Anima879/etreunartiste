package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.formes.VueRectangle;

import java.awt.event.MouseEvent;
import java.util.List;

/**
 * <p>
 * Méthode héritant de {@link Outil Outil}.
 * Permet de gérer l'affichage de la classe Forme dans la classe PanneauDessin.
 * </p>
 *
 * @see Outil
 * @see Forme
 * @see fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin
 */
public abstract class OutilForme extends Outil {
    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (e.getClickCount() == 2) {
            double x = this.getDebut().getAbscisse() + Forme.LARGEUR_PAR_DEFAUT;
            double y = this.getDebut().getOrdonnee() + Forme.HAUTEUR_PAR_DEFAUT;
            this.setFin(new Coordonnees(x, y));
            this.getPanneauDessin().ajouterVueForme(creerVueForme());
            this.getPanneauDessin().repaint();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);
        this.clearGhost();
        if (this.getDebut().getAbscisse() != this.getFin().getAbscisse() ||
                this.getDebut().getOrdonnee() != this.getFin().getOrdonnee()) {
            this.getPanneauDessin().ajouterVueForme(creerVueForme());
            this.getPanneauDessin().repaint();
        } else {
            System.out.println("Mouse didn't move");
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        super.mouseDragged(e);
        this.clearGhost();
        VueForme ghost = creerVueForme();
        ghost.setGhost(true);
        this.getPanneauDessin().ajouterVueForme(ghost);
        this.getPanneauDessin().repaint();
    }

    public void clearGhost() {
        int length = this.getPanneauDessin().getVueFormes().size();
        if (length != 0) {
            VueForme last = this.getPanneauDessin().getVueFormes().get(length - 1);
            if (last.isGhost())
                this.getPanneauDessin().getVueFormes().remove(length - 1);
        }
    }

    /**
     * <h1>Méthode abstraite</h1>
     * <p>
     * Créer la VueForme correspondant à la Forme donnée.
     * </p>
     * @return VueForme la vue de la forme.
     * @see VueForme
     */
    protected abstract VueForme creerVueForme();
}
