package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.vue.formes.VueCercle;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

import java.awt.event.MouseEvent;

/**
 * <p>
 * Classe gérant l'affichage et la construction d'un cercle.
 * </p>
 *
 * @see VueCercle
 * @see VueForme
 * @see Cercle
 */
public class OutilCercle extends OutilForme {
    public OutilCercle() {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
    }

    @Override
    protected VueForme creerVueForme() {
        double largeur = Math.abs(this.getFin().getAbscisse() - this.getDebut().getAbscisse());
        double hauteur = Math.abs(this.getFin().getOrdonnee() - this.getDebut().getOrdonnee());
        double diametre = Math.max(largeur, hauteur);
        double x, y;

        if (this.getDebut().getAbscisse() <= this.getFin().getAbscisse() &&
                this.getDebut().getOrdonnee() <= this.getFin().getOrdonnee()) {
            x = this.getDebut().getAbscisse();
            y = this.getDebut().getOrdonnee();
        } else if (this.getDebut().getAbscisse() > this.getFin().getAbscisse() &&
                this.getDebut().getOrdonnee() < this.getFin().getOrdonnee()) {
            x = this.getDebut().getAbscisse() - diametre;
            y = this.getDebut().getOrdonnee();
        } else if (this.getDebut().getAbscisse() > this.getFin().getAbscisse() &&
                this.getDebut().getOrdonnee() > this.getFin().getOrdonnee()) {
            x = this.getDebut().getAbscisse() - diametre;
            y = this.getDebut().getOrdonnee() - diametre;
        } else {
            x = this.getDebut().getAbscisse();
            y = this.getDebut().getOrdonnee() - diametre;
        }

        Cercle e = new Cercle(new Coordonnees(x, y), diametre);
        e.setCouleur(this.getPanneauDessin().getCouleurCourante());
        e.setRempli(this.getPanneauDessin().getModeRemplissage());
        System.out.println(e);
        return new VueCercle(e);
    }
}
