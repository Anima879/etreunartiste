package fr.eseo.poo.projet.artiste.xml;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

import javax.xml.xpath.XPathExpressionException;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.Remplissable;
import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.formes.*;

/**
 * L'enregistreur SVG est responsable de l'enregistrement d'un dessin au format
 * SVG (sous-ensemble du standard SVG).
 * 
 * Il utilise les classes java.io.FileWriter et java.io.Writer de l'API Java
 * standard qui permettent d'écrire très simplement dans un fichier texte.
 * 
 * Les méthodes ecrisXxxx devront être complétées.
 *
 */
public class EnregistreurSVG {

	/**
	 * Lance le test d'enregistrement (méthode teste) avec le fichier SVG
	 * d'entrée "Dessin-in.svg" et le fichier SVG de sortie "Dessin-out.svg".
	 * 
	 * Vérifier la conformité du fichier de sortie en l'affichant avec un
	 * navigateur Web et en comparant cet affichage avec celui fourni par la
	 * méthode LecteurSVG.main.
	 */
	public static void main(String[] args) throws IOException {
		EnregistreurSVG enregistreur = new EnregistreurSVG();
		enregistreur.teste("Dessin-in.svg", "Dessin-out.svg");
	}

	/**
	 * Teste l'enregistrement du dessin dans un fichier SVG. Le fichier SVG
	 * d'entrée est préalablement lu, puis sauvagardé dans un fichier de sortie.
	 * Le fichier de sortie est ensuite chargé et visualisé par l'application.
	 * 
	 * @param nomFichierEntree le nom du fichier SVG d'entrée lu
	 * @param nomFichierSortie le nom du fichier SVG de sortie écrit
	 * @throws FileNotFoundException si l'un des noms des fichiers n'est pas
	 *             valide
	 */
	public void teste(String nomFichierEntree, String nomFichierSortie) throws IOException {
		Cercle cercle = new Cercle(new Coordonnees(10, 10), 10);
		Ellipse ellipse = new Ellipse(new Coordonnees(100, 100), 100, 200);
		ellipse.setCouleur(Color.RED);
		Ligne ligne = new Ligne(new Coordonnees(3.14, 20), 5, 4);
		Etoile etoile = new Etoile(new Coordonnees(), 100);
		List<VueForme> dessin = Arrays.asList(new VueCercle(cercle), new VueLigne(ligne), new VueEtoile(etoile),
				new VueEllipse(ellipse));
		enregistreDessin(nomFichierSortie, dessin);
	}

	/**
	 * Enregistre le dessin donné dans un fichier.
	 * 
	 * @param nomFichier le nom du fichier de sauvegarde
	 * @param dessin le dessin formé de la liste des vues des formes
	 * @throws IOException si l'écriture échoue
	 */
	public void enregistreDessin(String nomFichier, List<VueForme> dessin) throws IOException {
		Writer redacteur = new FileWriter(nomFichier);
		redacteur.write("<?xml version='1.0' encoding='UTF-8' ?>\n");
		ecrisDessin(dessin, redacteur);
		redacteur.close();
	}

	/**
	 * Ecris chaque forme du dessin
	 * 
	 * @param dessin Le dessin à écrire
	 * @param redacteur Le rédacteur SVG
	 * @throws IOException si l'écriture échoue
	 */
	public void ecrisDessin(List<VueForme> dessin, Writer redacteur) throws IOException {
		redacteur.write("<svg xmlns='http://www.w3.org/2000/svg'>\n");
		for (VueForme vueForme : dessin) {
			ecrisForme(vueForme.getForme(), redacteur);
		}
		redacteur.write("</svg>");
	}

	/**
	 * Ecris la forme. Cette méthode invoque les méthodes ecris<Forme> en
	 * fonction du type de la forme.
	 *
	 * @param redacteur Le rédacteur SVG
	 * @throws IOException si l'écriture échoue
	 */
	public void ecrisForme(Forme forme, Writer redacteur) throws IOException {
		String nom = forme.getClass().getSimpleName();
		switch (nom) {
			case "Ellipse":
				nom = "ellipse";
				break;
			case "Cercle":
				nom = "circle";
				break;
			case "Ligne":
				nom = "line";
				break;
			case "Etoile":
				nom = "polygon";
				break;
			case "Trace":
				// TODO à modifier
				nom = "(à définir)";
				break;
		}
		redacteur.write("\t<" + nom + " ");
		switch (nom) {
			case "ellipse":
				ecrisEllipse((Ellipse) forme, redacteur);
				break;
			case "circle":
				ecrisCercle((Cercle) forme, redacteur);
				break;
			case "line":
				ecrisLigne((Ligne) forme, redacteur);
				break;
			case "polygon":
				ecrisEtoile((Etoile) forme, redacteur);
				break;
		}
		// définition de la couleur de la forme
		Color c = forme.getCouleur();
		String couleur = "#" + String.format("%02X", c.getRed()) + String.format("%02X", c.getGreen()) +
				String.format("%02X", c.getBlue());
		String fill = "none";
		if (forme instanceof Remplissable)
			fill = (((Remplissable) forme).estRempli()) ? couleur : "none";

		redacteur.write("fill='" + fill + "' stroke='" + couleur + "'");
		redacteur.write("/>\n");
	}

	/**
	 * Ecris l'ellispe.
	 * 
	 * @param forme la forme à écrire
	 * @param redacteur Le rédacteur SVG
	 * @throws IOException si l'écriture échoue
	 */
	public void ecrisEllipse(Ellipse forme, Writer redacteur) throws IOException {
		double rx = forme.getLargeur() / 2;
		double ry = forme.getHauteur() / 2;
		double cx = forme.getCentre().getAbscisse();
		double cy = forme.getCentre().getOrdonnee();
		redacteur.write("cx=\"" + cx + "\" cy=\"" + cy + "\" rx=\"" + rx + "\" ry=\"" + ry + "\" ");
	}

	/**
	 * Ecris le cercle.
	 * 
	 * @param forme la forme à écrire
	 * @param redacteur Le rédacteur SVG
	 * @throws IOException si l'écriture échoue
	 */
	public static void ecrisCercle(Cercle forme, Writer redacteur) throws IOException {
		double r = forme.getLargeur() / 2;
		double cx = forme.getCentre().getAbscisse();
		double cy = forme.getCentre().getOrdonnee();
		redacteur.write("cx=\"" + cx + "\" cy=\"" + cy + "\" r=\"" + r + "\" ");
	}

	/**
	 * Ecris la ligne.
	 * 
	 * @param forme la forme à écrire
	 * @param redacteur Le rédacteur SVG
	 * @throws IOException si l'écriture échoue
	 */
	public static void ecrisLigne(Ligne forme, Writer redacteur) throws IOException {
		double x1 = forme.getC1().getAbscisse();
		double y1 = forme.getC1().getOrdonnee();
		double x2 = forme.getC2().getAbscisse();
		double y2 = forme.getC2().getOrdonnee();
		redacteur.write("x1=\"" + x1 + "\" y1=\"" + y1 + "\" x2=\"" + x2 + "\" y2=\"" + y2 + "\" ");
	}

	/**
	 * Ecris l'étoile.
	 * 
	 * @param forme la forme à écrire
	 * @param redacteur Le rédacteur SVG
	 * @throws IOException si l'écriture échoue
	 */
	public void ecrisEtoile(Etoile forme, Writer redacteur) throws IOException {
		redacteur.write("points=\"");
		double x, y;
		for (Coordonnees point : forme.getCoordonnees()) {
			x = point.getAbscisse();
			y = point.getOrdonnee();
			redacteur.write(x + "," + y + " ");
		}
		redacteur.write("\" ");
	}

}
