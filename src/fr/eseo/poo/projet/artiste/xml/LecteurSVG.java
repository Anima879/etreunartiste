package fr.eseo.poo.projet.artiste.xml;

import static javax.xml.xpath.XPathConstants.NODESET;
import static javax.xml.xpath.XPathConstants.NUMBER;
import static javax.xml.xpath.XPathConstants.STRING;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import fr.eseo.poo.projet.artiste.modele.Remplissable;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.formes.VueCercle;
import fr.eseo.poo.projet.artiste.vue.formes.VueEllipse;
import fr.eseo.poo.projet.artiste.vue.formes.VueEtoile;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;
import fr.eseo.poo.projet.artiste.vue.formes.VueLigne;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;
import sun.reflect.generics.factory.CoreReflectionFactory;

/**
 * Un lecteur SVG est un processeur DOM/XPath responsable du chargement d'un
 * dessin au format SVG.
 * <p>
 * Les méthodes lisDessin et lisXxxx devront être complétées.
 */
public class LecteurSVG {

    /**
     * Lance le test de chargement (méthode teste) avec le fichier SVG
     * "Dessin-in.svg".
     */
    public static void main(String[] args) throws XPathExpressionException {
        LecteurSVG lecteur = new LecteurSVG();
        lecteur.teste("Dessin-out.svg");
    }

    /**
     * Evaluateur d'expressions XPath permettant de naviguer facilement dans le
     * document DOM.
     */
    private final XPath xpath = XPathFactory.newInstance().newXPath();

    /**
     * Teste le chargement du fichier SVG. Le contenu du fichier est ensuite
     * affiché dans la fenêtre de l'application (classe FenêtreBeAnArtist).
     *
     * @param nomFichier le fichier d'entrée à lire
     * @throws XPathExpressionException si l'expression XPath est erronée
     */
    public void teste(String nomFichier) throws XPathExpressionException {
        final List<VueForme> dessin = lisDessin(nomFichier);
        SwingUtilities.invokeLater(() -> {
            // création du panneau de dessin
            JFrame fenetre = new JFrame("Etre Un Artiste");
            PanneauDessin panneauDessin = new PanneauDessin(600, 600, new Color(255, 255, 255));
            fenetre.add(panneauDessin);
            fenetre.pack();
            fenetre.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            fenetre.setLocationRelativeTo(null);
            // ajout des formes dans le panneau
            for (VueForme vueForme : dessin) {
                panneauDessin.ajouterVueForme(vueForme);
            }
            fenetre.setVisible(true);
        });
    }

    /**
     * Charge le fichier SVG donné dans un document DOM puis renvoie
     * l'intégralité du dessin sous la forme d'une liste de vues représentant
     * les formes stockées dans le fichier.
     *
     * @param nomFichier le nom du fichier SVG
     * @return l'intégralité du dessin sous la forme d'une liste de vues
     * @throws XPathExpressionException si l'expression XPath est erronée
     */
    public List<VueForme> lisDessin(String nomFichier) throws XPathExpressionException {
        List<VueForme> dessin = new ArrayList<>();
        Document document = new ChargeurDOM().charge(nomFichier);
        // L'expression XPath doit renvoyer les listes des noeuds DOM représentant toutes les formes
        // présentes dans le dessin.
        NodeList figures = (NodeList) xpath.evaluate("/svg/child::node()", document, NODESET);
        for (int i = 0; i < figures.getLength(); i++) {
            Node noeud = figures.item(i);
            VueForme vue = lisVueForme(noeud);
            // note: la condition suivante "vue != null" permet de lancer le "main" bien que toutes
            // les méthodes lis<Forme> ne soient pas encore implémentées.
            if (vue != null) {
                dessin.add(vue);
            }
        }
        return dessin;
    }

    /**
     * Crée une forme et sa vue associée réprésentées par le noeud DOM donné,
     * puis renvoie cette vue. Cette méthode invoque les méthodes lis<Forme>
     * définies pour chacune des <Forme> considérée.
     *
     * @param noeud le noeud représentant la vue et sa forme
     * @return la vue stockée dans le noeud considéré
     */
    public VueForme lisVueForme(Node noeud) throws XPathExpressionException {
        VueForme vue = null;
        switch (noeud.getNodeName()) {
            case "ellipse":
                vue = new VueEllipse(lisEllipse(noeud));
                break;
            case "circle":
                vue = new VueCercle(lisCercle(noeud));
                break;
            case "line":
                vue = new VueLigne(lisLigne(noeud));
                break;
            case "polygon":
                vue = new VueEtoile(lisEtoile(noeud));
                break;
        }
        if (vue != null) {
            vue.getForme().setCouleur(lisCouleur(noeud));
            if (vue.getForme() instanceof Remplissable) {
                ((Remplissable) vue.getForme()).setRempli(lisRemplissage(noeud));
            }
        }

        return vue;
    }

    /**
     * Renvoie une nouvelle ellipse représentée par le noeud DOM donné.
     *
     * @param noeud le noeud représentant l'ellipse
     * @return l'ellipse stockée dans le noeud considéré
     */
    public Ellipse lisEllipse(Node noeud) throws XPathExpressionException {
        // La 1ère initialisation est fournie.
        double cx = (double) xpath.evaluate("@cx", noeud, NUMBER);
        double cy = (double) xpath.evaluate("@cy", noeud, NUMBER);
        double rx = (double) xpath.evaluate("@rx", noeud, NUMBER);
        double ry = (double) xpath.evaluate("@ry", noeud, NUMBER);
        Coordonnees position = new Coordonnees(cx, cy);
        return new Ellipse(position, 2 * rx, 2 * ry);
    }

    /**
     * Renvoie un nouveau cercle représenté par le noeud DOM donné.
     *
     * @param noeud le noeud représentant le cercle
     * @return le cercle stocké dans le noeud considéré
     */
    public Cercle lisCercle(Node noeud) {
        double cx = Double.parseDouble(noeud.getAttributes().getNamedItem("cx").getNodeValue());
        double cy = Double.parseDouble(noeud.getAttributes().getNamedItem("cy").getNodeValue());
        double r = Double.parseDouble(noeud.getAttributes().getNamedItem("r").getNodeValue());
        Coordonnees position = new Coordonnees(cx, cy);
        return new Cercle(position, 2 * r);
    }

    /**
     * Renvoie la nouvelle ligne représentée par le noeud DOM donné.
     *
     * @param noeud le noeud représentant la ligne
     * @return la ligne stockée dans le noeud considéré
     */
    public Ligne lisLigne(Node noeud) {
        double x1 = Double.parseDouble(noeud.getAttributes().getNamedItem("x1").getNodeValue());
        double y1 = Double.parseDouble(noeud.getAttributes().getNamedItem("y1").getNodeValue());
        double x2 = Double.parseDouble(noeud.getAttributes().getNamedItem("x2").getNodeValue());
        double y2 = Double.parseDouble(noeud.getAttributes().getNamedItem("y2").getNodeValue());
        Coordonnees p1 = new Coordonnees(x1, y1);
        Coordonnees p2 = new Coordonnees(x2, y2);
        Ligne ligne = new Ligne();
        ligne.setC1(p1);
        ligne.setC2(p2);
        return ligne;
    }

    /**
     * Renvoie la nouvelle étoile représentée par le noeud DOM donné.
     *
     * @param noeud le noeud représentant l'étoile
     * @return l'étoile stockée dans le noeud considéré
     */
    public Etoile lisEtoile(Node noeud) {
        String points = noeud.getAttributes().getNamedItem("points").getNodeValue();
        StringTokenizer tokenizer = new StringTokenizer(points, ", ");
        List<Coordonnees> coordonnees = new ArrayList<>();
        while (tokenizer.hasMoreTokens()) {
            double x = Double.parseDouble(tokenizer.nextToken());
            double y = Double.parseDouble(tokenizer.nextToken());
            coordonnees.add(new Coordonnees(x, y));
        }
        // calcul de l'étoile :-)
        return calculeEtoile(coordonnees);
    }

    /**
     * Renvoie une nouvelle étoile calculée à partir de la liste de ses
     * coordonnées.
     * <p>
     * Il faut "un peu" réflechir et donc s'aider du trio "feuille, stylo,
     * cerveau".
     * <p>
     * Indications : calculer les coordonnéeds du centre de l'étoile ; calculer
     * le(s) rayon(s) en utilisant la méthode Coordonnees.distanceVers ;
     * calculer le(s) angle(s) en utilisant la méthode Coordonnees.angleVers.
     *
     * @param points La liste des coordonnées des points de l'étoile
     * @return L'étoile calculée à partir de la liste de ses coordonnées
     */
    public Etoile calculeEtoile(List<Coordonnees> points) {
        int nombreBranches = points.size() / 2;

        double x = 0.0, y = 0.0;
        for (int i = 0; i < points.size(); i += 2) {
            x += points.get(i).getAbscisse();
            y += points.get(i).getOrdonnee();
        }
        x /= nombreBranches;
        y /= nombreBranches;

        Coordonnees centre = new Coordonnees(x, y);
        double rayon = centre.distanceVers(points.get(0));
        Coordonnees position = new Coordonnees(centre.getAbscisse() - rayon, centre.getOrdonnee() - rayon);
        double taille = rayon * 2;
        double anglePremiereBranche = centre.angleVers(points.get(0));
        double longueurBranche = (rayon - centre.distanceVers(points.get(1))) / rayon;
        return new Etoile(position, taille, nombreBranches, anglePremiereBranche, longueurBranche);
    }

    /**
     * Convertis une couleur représentée par une chaîne de caractères en une
     * couleur compatible avec l'API Java. La chaîne en question est au format
     * "#RRVVBB" où RR représente la valeur héxadécimale de la composante rouge,
     * VV celle de la composante verte et BB celle de la composante bleue.
     *
     * @return la couleur compatible avec l'API Java
     */
    public Color lisCouleur(Node noeud) throws XPathExpressionException {
        String couleur = (String) xpath.evaluate("@stroke", noeud, STRING);

        try {
            return Color.decode(couleur);
        } catch (NumberFormatException e) {
            return Forme.COULEUR_PAR_DEFAUT;
        }
    }

    /**
     * Renvoie {@code true} si la forme est remplie.
     * @param noeud noeud contexte SVG de la forme
     * @return {@code true} si l'attribue {@code fill} est différent de {@code none}
     * @throws XPathExpressionException si l'expression XPath lève une erreur
     */
    public boolean lisRemplissage(Node noeud) throws XPathExpressionException {
        return !(xpath.evaluate("@fill", noeud, STRING)).equals("none");
    }
}
