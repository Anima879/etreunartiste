package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Ligne extends Forme {

    public static final double EPSILON = 1e-1;

    public Ligne(Coordonnees position, double largeur, double hauteur) {
        super(position, largeur, hauteur);
    }

    public Ligne(double largeur, double hauteur) {
        super(largeur, hauteur);
    }

    public Ligne(Coordonnees position) {
        super(position);
    }

    public Ligne(Coordonnees pos1, Coordonnees pos2) {
        super(pos1);
        this.setLargeur(pos2.getAbscisse() - pos1.getAbscisse());
        this.setHauteur(pos2.getOrdonnee() - pos1.getOrdonnee());
    }

    public Ligne() {
        super();
    }

    public Coordonnees getC1() {
        return this.getPosition();
    }

    public Coordonnees getC2() {
        return new Coordonnees(this.getPosition().getAbscisse() + this.getLargeur(),
                this.getPosition().getOrdonnee() + this.getHauteur());
    }

    public void setC1(Coordonnees position) {
        this.setLargeur(this.getC2().getAbscisse() - position.getAbscisse());
        this.setHauteur(this.getC2().getOrdonnee() - position.getOrdonnee());
        this.setPosition(position);
    }

    public void setC2(Coordonnees position) {
        this.setLargeur(position.getAbscisse() - this.getPosition().getAbscisse());
        this.setHauteur(position.getOrdonnee() - this.getPosition().getOrdonnee());
    }

    @Override
    public double getCadreMinX() {
        return Math.min(this.getC1().getAbscisse(), this.getC2().getAbscisse());
    }

    @Override
    public double getCadreMinY() {
        return Math.min(this.getC1().getOrdonnee(), this.getC2().getOrdonnee());
    }

    @Override
    public double getCadreMaxX() {
        return this.getCadreMinX() + Math.abs(this.getLargeur());
    }

    @Override
    public double getCadreMaxY() {
        return this.getCadreMinY() + Math.abs(this.getHauteur());
    }

    @Override
    public double aire() {
        return 0;
    }

    @Override
    public double perimetre() {
        return Math.sqrt(Math.pow(this.getLargeur(), 2) + Math.pow(this.getHauteur(), 2));
    }

    public double getAngle() {
        double angleRadian = this.getC1().angleVers(this.getC2());
        if (angleRadian < 0) {
            return 360 + angleRadian * (180 / Math.PI);
        } else {
            return angleRadian * (180 / Math.PI);
        }
    }

    @Override
    public boolean contient(Coordonnees point) {
        return (this.getC1().distanceVers(point) + point.distanceVers(this.getC2()) - this.perimetre()) <= EPSILON;
    }

    @Override
    public String toString() {
        DecimalFormat df = (DecimalFormat)NumberFormat.getNumberInstance();
        df.applyPattern("###0.0#");
        return "[Ligne] c1 : " + this.getPosition().toString() +
                " c2 : " + this.getC2().toString() + " longueur : " +
                df.format(this.perimetre()) + " angle : " +
                df.format(this.getAngle()) + "° " + this.toStringColor();
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
