package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.Remplissable;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe modélisant un polygone régulier étoilé.
 *
 * @author Eloi Mahé
 */
public class Etoile extends Forme implements Remplissable {

    public static final double EPSILON = 1e-1;

    public static final int NOMBRE_BRANCHES_PAR_DEFAUT = 4;
    public static final double ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT = 0;
    public static final double LONGUEUR_BRANCHE_PAR_DEFAUT = 0.5;

    private List<Coordonnees> coordonnees;
    private int nombreBranches;
    private double anglePremiereBranche;
    private double longueurBranche;
    private boolean rempli;

    /**
     * <h1>Constructeur exhaustif</h1>
     *
     * @param pos              Position de la forme
     * @param taille           Taille de la forme
     * @param nbrBranche       Nombre de branche de l'étoile
     * @param anglePremBranche Angle de la première branche de l'étoile dans le sens horaire par rapport à
     *                         l'horizontal
     * @param longBranche      Longueur des branches de l'étoile
     */
    public Etoile(Coordonnees pos, double taille, int nbrBranche, double anglePremBranche, double longBranche) {
        super(pos, taille, taille);

        if (anglePremBranche < -Math.PI || anglePremBranche > Math.PI)
            throw new IllegalArgumentException();
        this.anglePremiereBranche = anglePremBranche;

        if (taille < 0)
            throw new IllegalArgumentException();

        if (nbrBranche < 3 || nbrBranche > 15)
            throw new IllegalArgumentException();
        this.nombreBranches = nbrBranche;

        if (longBranche <= 0 || longBranche >= 1)
            throw new IllegalArgumentException();
        this.longueurBranche = longBranche;

        this.calculeSommet();
        this.rempli = false;
    }

    public Etoile(Coordonnees coordonnees, double taille) {
        this(coordonnees, taille, NOMBRE_BRANCHES_PAR_DEFAUT, ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT,
                LONGUEUR_BRANCHE_PAR_DEFAUT);
    }

    public Etoile(Coordonnees coordonnees) {
        this(coordonnees, Forme.LARGEUR_PAR_DEFAUT);
    }

    public Etoile(double taille) {
        this(new Coordonnees(), taille);
    }

    public Etoile() {
        this(new Coordonnees(), Forme.LARGEUR_PAR_DEFAUT);
    }

    /**
     * Calcule les coordonnées de chaque sommet de l'étoile.
     */
    public void calculeSommet() {
        this.coordonnees = new ArrayList<>();
        double angle = this.anglePremiereBranche;
        double step = Math.PI / this.nombreBranches;
        double x, y;
        for (int i = 0; i < this.nombreBranches * 2; i++) {
            if (i % 2 == 0) {
                x = this.getRayon() * Math.cos(angle);
                y = this.getRayon() * Math.sin(angle);

            } else {
                x = (this.getRayon() * (1 - this.getLongueurBranche())) * Math.cos(angle);
                y = (this.getRayon() * (1 - this.getLongueurBranche())) * Math.sin(angle);
            }

            x += this.getCentre().getAbscisse();
            y += this.getCentre().getOrdonnee();
            this.coordonnees.add(new Coordonnees(x, y));
            angle += step;
        }
    }

    public double getRayon() {
        return this.getLargeur() / 2;
    }

    public List<Coordonnees> getCoordonnees() {
        return coordonnees;
    }

    public void setCoordonnees(List<Coordonnees> coordonnees) {
        this.coordonnees = coordonnees;
    }

    public int getNombreBranches() {
        return nombreBranches;
    }

    public void setNombreBranches(int nombreBranches) {
        if (nombreBranches < 3 || nombreBranches > 15)
            throw new IllegalArgumentException();
        this.nombreBranches = nombreBranches;
        this.calculeSommet();
    }

    public double getAnglePremiereBranche() {
        return anglePremiereBranche;
    }

    public void setAnglePremiereBranche(double anglePremiereBranche) {
        if (anglePremiereBranche < -Math.PI || anglePremiereBranche > Math.PI)
            throw new IllegalArgumentException();
        this.anglePremiereBranche = anglePremiereBranche;
        this.calculeSommet();
    }

    public double getLongueurBranche() {
        return longueurBranche;
    }

    public void setLongueurBranche(double longueurBranche) {
        if (longueurBranche <= 0 || longueurBranche >= 1)
            throw new IllegalArgumentException();

        this.longueurBranche = longueurBranche;
        this.calculeSommet();
    }

    public Coordonnees getCentre() {
        double x = this.getPosition().getAbscisse() + this.getLargeur() / 2;
        double y = this.getPosition().getOrdonnee() + this.getLargeur() / 2;
        return new Coordonnees(x, y);
    }

    @Override
    public void setLargeur(double largeur) {
        if (largeur <= 0)
            throw new IllegalArgumentException();

        super.setLargeur(largeur);
        super.setHauteur(largeur);
        this.calculeSommet();
    }

    @Override
    public void setHauteur(double hauteur) {
        if (hauteur <= 0)
            throw new IllegalArgumentException();

        super.setHauteur(hauteur);
        super.setLargeur(hauteur);
        this.calculeSommet();
    }

    /**
     * <h2>Calcul l'aire de l'étoile</h2>
     *
     * @return aire de l'étoile
     */
    @Override
    public double aire() {
        double alpha = Math.PI * 2 / this.nombreBranches;
        double c = this.getRayon() * (1 - this.getLongueurBranche());
        double b = 2 * c * Math.sin(alpha / 2);
        double he = this.getRayon() - c * Math.cos(alpha / 2);

        return this.nombreBranches * c * Math.cos(alpha / 2) * b / 2 + this.nombreBranches * b * he / 2;
    }

    /**
     * <h2>Calcule le périmètre de l'étoile</h2>
     *
     * @return périmètre de l'étoile
     */
    @Override
    public double perimetre() {
        double alpha = Math.PI * 2 / this.nombreBranches;
        double c = this.getRayon() * (1 - this.getLongueurBranche());
        double b = 2 * c * Math.sin(alpha / 2);
        double hi = c * Math.cos(alpha / 2);
        double he = this.getRayon() - hi;

        double ci = Math.sqrt(Math.pow(he, 2) + Math.pow(b / 2, 2));

        return 2 * this.nombreBranches * ci;
    }

    /**
     * <p>
     * Retourne <code>true</code> si le point en paramètre est contenu dans l'étoile. <code>false</code> sinon.
     * </p>
     * <p>
     * Utilise la méthode de la projection des rayons :
     * </p>
     * <ul>
     *     <li>Projette 4 rayons dans quatre directions à partir du point</li>
     *     <li>Si les quatres rayons rencontrent un bord de l'étoile, le point est contenu.</li>
     *     <li>Si au moins un rayon ne rencontre aucune bordure, alors le point n'est pas contenu</li>
     * </ul>
     * <p>
     *     Pour connaître l'intersection entre un segement et une demi-droite :
     *     <a href="https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection">Wikipédia</a>
     * </p>
     *
     * @param point Coordonnees du point à vérifié.
     * @return si le point est contenu ou non
     */
    @Override
    public boolean contient(Coordonnees point) {
        double x = point.getAbscisse();
        double y = point.getOrdonnee();

        // Liste des rayons
        double[] listXr = {x + 1, x, x - 1, x};
        double[] listYr = {y, y + 1, y, y - 1};

        // Coordonnées des extrémités de chaque bordure
        double x1, x2, y1, y2;
        // Index du point suivant dans la lsite des coordonnées
        int j;
        // Coordonnées d'un rayon
        double xr, yr;
        // Paramètres permettant de vérifier si une droite intersecte un segment
        double den, t, u;

        // On parcoure chaque rayon
        for (int k = 0; k < 4; k++) {

            xr = listXr[k];
            yr = listYr[k];

            // Pour chaque rayon, on regarde chaque mur
            for (int i = 0; i < this.coordonnees.size(); i++) {
                j = (i == (this.coordonnees.size() - 1)) ? 0 : i + 1;

                // Si le point est assez proche d'une bordure, on considère que le point est contenu.
                Ligne bordure = new Ligne(this.coordonnees.get(i), this.coordonnees.get(j));
                if (bordure.contient(point))
                    return true;

                x1 = this.coordonnees.get(i).getAbscisse();
                y1 = this.coordonnees.get(i).getOrdonnee();
                x2 = this.coordonnees.get(j).getAbscisse();
                y2 = this.coordonnees.get(j).getOrdonnee();

                den = (x1 - x2) * (y - yr) - (y1 - y2) * (x - xr);
                // Si den égale zéro, la bordure et le rayon son parallèle.
                if (den == 0) {
                    continue;
                }

                t = ((x1 - x) * (y - yr) - (y1 - y) * (x - xr)) / den;
                u = -((x1 - x2) * (y1 - y) - (y1 - y2) * (x1 -  x)) / den;

                // Tolérance de la proximité à zéro
                if (Math.abs(t) < EPSILON) {
                    t = 0;
                }
                if (Math.abs(u) < EPSILON) {
                    u = 0;
                }

                if (u >= 0 && t >= 0 && t <= 1 + EPSILON) {
                    break;
                } else if (i == this.coordonnees.size() - 1) {
                    return false;
                }
            }
        }

        return true;
    }

    @Override
    public boolean estRempli() {
        return this.rempli;
    }

    @Override
    public void setRempli(boolean modeRemplissage) {
        this.rempli = modeRemplissage;
    }

    @Override
    public String toString() {
        String pattern = "###0.0#";
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance();
        df.applyPattern(pattern);

        return "[Etoile" + ((this.rempli) ? "-Rempli" : "") + "] : pos " + this.getPosition().toString() +
                " dim " + df.format(this.getLargeur()) + " x " + df.format(this.getHauteur()) +
                " périmètre : " + df.format(this.perimetre()) + " aire : " +
                df.format(this.aire()) + " " + this.toStringColor();
    }
}
