package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * <p>
 *     Classe modélisant un cercle.
 * </p>
 * <p>
 *     Hérite de {@link Ellipse Ellipse}
 * </p>
 */
public class Cercle extends Ellipse {

    public Cercle(Coordonnees position, double diametre) {
        super(position, diametre, diametre);
    }

    public Cercle(Coordonnees position) {
        super(position, Forme.LARGEUR_PAR_DEFAUT, Forme.LARGEUR_PAR_DEFAUT);
    }

    public Cercle(double taille) {
        super(taille, taille);
    }

    public Cercle() {
        super(Forme.LARGEUR_PAR_DEFAUT, Forme.LARGEUR_PAR_DEFAUT);
    }

    @Override
    public boolean contient(Coordonnees point) {
        return this.getCentre().distanceVers(point) <= this.getHauteur() / 2;
    }

    @Override
    public void setLargeur(double largeur) {
        if (largeur < 0) {
            throw new IllegalArgumentException();
        }
        super.setLargeur(largeur);
        super.setHauteur(largeur);
    }

    @Override
    public void setHauteur(double hauteur) {
        if (hauteur < 0) {
            throw new IllegalArgumentException();
        }
        super.setLargeur(hauteur);
        super.setHauteur(hauteur);
    }

    @Override
    public double perimetre() {
        return 2 * Math.PI * (this.getHauteur() / 2);
    }

    @Override
    public String toString() {
        String pattern = "###0.0#";
        NumberFormat nf = NumberFormat.getNumberInstance();
        DecimalFormat df = (DecimalFormat)nf;
        df.applyPattern(pattern);

        String stringRempli = this.rempli ? "-Rempli":"";

        return "[Cercle" + stringRempli + "] : pos " + this.getPosition().toString() +
                " dim " + df.format(this.getLargeur()) + " x " + df.format(this.getHauteur()) +
                " périmètre : " + df.format(this.perimetre()) + " aire : " +
                df.format(this.aire()) + " " + this.toStringColor();
    }
}
