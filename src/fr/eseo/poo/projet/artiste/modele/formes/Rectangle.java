package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.Remplissable;


public class Rectangle extends Forme implements Remplissable {

    private boolean rempli;

    public Rectangle(Coordonnees position, double largeur, double hauteur) {
        super(position, largeur, hauteur);
        if (this.getLargeur() < 0 || this.getHauteur() < 0) {
            throw new IllegalArgumentException();
        }
    }

    public Rectangle(double largeur, double hauteur) {
        super(largeur, hauteur);
        if (this.getLargeur() < 0 || this.getHauteur() < 0) {
            throw new IllegalArgumentException();
        }
    }

    public Rectangle(Coordonnees position) {
        super(position);
    }

    public Rectangle() {
        super();
    }

    @Override
    public double aire() {
        return this.getHauteur() * this.getLargeur();
    }

    @Override
    public double perimetre() {
        return this.getHauteur() * 2 + this.getLargeur() * 2;
    }

    @Override
    public boolean contient(Coordonnees point) {
        return point.getAbscisse() >= this.getCadreMinX() && point.getAbscisse() <= this.getCadreMaxX() &&
                point.getOrdonnee() >= this.getCadreMinY() && point.getOrdonnee() <= this.getCadreMaxY();
    }

    @Override
    public boolean estRempli() {
        return this.rempli;
    }

    @Override
    public void setRempli(boolean modeRemplissage) {
        this.rempli = modeRemplissage;
    }
}
