package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coloriable;
import fr.eseo.poo.projet.artiste.modele.Coordonnees;

import javax.swing.UIManager;
import java.awt.Color;
import java.awt.Panel;
import java.util.Locale;

/**
 * <h1>Classe abstraite Forme</h1>
 *  <p>Classe abstraite utilisée pour modéliser sommairement toutes les formes géométriques du logiciel.
 *  Toutes les formes héritent et implémentent cette classe.</p>
 *
 * @author Eloi Mahé
 * @since 06/04/2020
 * @version 1.0
 */
public abstract class Forme implements Coloriable {

    /**
     * <h1>Constante de classe</h1>
     * <p>
     *     Largeur par défaut des objets Forme.
     * </p>
     */
    public static final double LARGEUR_PAR_DEFAUT = 10;

    /**
     * <h1>Constante de classe</h1>
     * <p>
     *     Hauteur par défaut des objets Forme.
     * </p>
     */
    public static final double HAUTEUR_PAR_DEFAUT = 10;

    /**
     * <h1>Constante de classe</h1>
     * <p>
     *     Couleur par défaut de la Forme.
     * </p>
     * @see UIManager
     * @see Panel
     * @see Color
     */
    public static final Color COULEUR_PAR_DEFAUT = UIManager.getColor("Panel.foreground");

    /**
     * <h1>Attribut</h1>
     * <p>
     *     Largeur de la Forme.
     * </p>
     */
    private double largeur;
    /**
     * <h1>Attribut</h1>
     * <p>
     *     Hauteur de la Forme.
     * </p>
     */
    private double hauteur;
    /**
     * <h1>Attribut</h1>
     * <p>
     *     Coordonnées de la forme implémentées par la classe Coordonnees.
     * </p>
     * @see Coordonnees
     */
    private Coordonnees position;
    /**
     * <h1>Attribut</h1>
     * <p>
     *     Couleur de la forme.
     *     Les méthodes sont implémentées à partir de l'interface {@link Coloriable Coloriable} :
     *     <ul>
     *         <li>{@link #getCouleur() Accesseur}</li>
     *         <li>{@link #setCouleur(Color) Mutateur}</li>
     *     </ul>
     * </p>
     * @see Color
     * @see Coloriable
     */
    private Color couleur;

    /**
     * <h1>Constructeur exhaustif</h1>
     * @see Coordonnees
     * @param pos coordonnées de la Forme.
     * @param largeur de la Forme
     * @param hauteur de la Forme
     */
    public Forme(Coordonnees pos, double largeur, double hauteur) {
        this.position = pos;
        this.largeur = largeur;
        this.hauteur = hauteur;
        this.couleur = COULEUR_PAR_DEFAUT;
    }

    /**
     * <h1>Constructeur à dimension</h1>
     * <p>
     *     Constructeur utilisant les coordonnées par défaut.
     * </p>
     * @see Coordonnees
     * @param largeur de la Forme
     * @param hauteur de la Forme
     */
    public Forme(double largeur, double hauteur) {
        this(new Coordonnees(), largeur, hauteur);
    }

    /**
     * <h1>Constructeur à coordonnées</h1>
     * <p>
     *     Constructeur utilisant la largeur et la hauteur par défaut.
     * </p>
     * @see Coordonnees
     * @param pos coordonnées de la Forme.
     */
    public Forme(Coordonnees pos) {
        this(pos, LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT);
    }

    /**
     * <h1>Constructeur par défaut</h1>
     * <p>
     *     Utilise le constructeur par défaut de Coordonnees
     * </p>
     * @see Coordonnees
     */
    public Forme() {
        this(new Coordonnees(), LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT);
    }

    /**
     * <p>
     *     Méthode abstraite calculant l'aire de la forme.
     * </p>>
     * @return aire de la forme.
     */
    public abstract double aire();

    /**
     * <p>
     *     Méthode abstraite calculant le périmètre de la forme.
     * </p>
     * @return périmètre de la forme.
     */
    public abstract double perimetre();

    /**
     * <p>
     *     Méthode abstraite vérifiant si un point implémenté par Coordonnees est dans la Forme ou non.
     * </p>
     * @see Coordonnees
     * @param point Coordonnees du point à vérifié.
     * @return boolean : true si le point est contenu dans la Forme, false sinon.
     */
    public abstract boolean contient(Coordonnees point);

    //-------------------
    //GETTERS AND SETTER
    //-------------------

    public double getLargeur() {
        return largeur;
    }

    public void setLargeur(double largeur) {
        this.largeur = largeur;
    }

    public double getHauteur() {
        return hauteur;
    }

    public void setHauteur(double hauteur) {
        this.hauteur = hauteur;
    }

    public Coordonnees getPosition() {
        return position;
    }

    public void setPosition(Coordonnees position) {
        this.position = position;
    }

    public double getCadreMinX() {
        return this.position.getAbscisse();
    }

    public double getCadreMinY() {
        return this.position.getOrdonnee();
    }

    public double getCadreMaxX() {
        return getCadreMinX() + largeur;
    }

    public double getCadreMaxY() {
        return getCadreMinY() + hauteur;
    }

    /**
     * <p>
     *     Déplace les coordonnées de la Forme
     * </p>
     * @param newPosition nouvelle position de la forme.
     */
    public void deplacerVers(Coordonnees newPosition) {
        this.position = newPosition;
    }

    /**
     * <h1>Surcharge de deplacerVers</h1>
     * <p>
     *     Permet de directement spécifier l'abscisse et l'ordonnée du nouveau point.
     * </p>
     * @param x abscisse du nouveau point
     * @param y ordonnée du nouveau point
     */
    public void deplacerVers(double x, double y) {
        this.position.deplacerVers(x, y);
    }

    /**
     * <p>
     *     Déplace les coordonnées de la Forme selon un vecteur (dx, dy).
     * </p>
     * @param dx composante x du vecteur
     * @param dy composante y du vecteur
     */
    public void deplacerDe(double dx, double dy) {
        this.position.deplacerDe(dx, dy);
    }

    @Override
    public Color getCouleur() {
        return this.couleur;
    }

    @Override
    public void setCouleur(Color couleur) {
        this.couleur = couleur;
    }

    public String toStringColor() {
        int red = this.couleur.getRed();
        int green = this.couleur.getGreen();
        int blue = this.couleur.getBlue();

        if (Locale.getDefault().getLanguage().equals(new Locale("fr").getLanguage())) {
            return "couleur = R" + red + ",V" + green + ",B" + blue;
        } else {
            return "couleur = R" + red + ",G" + green + ",B" + blue;
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
