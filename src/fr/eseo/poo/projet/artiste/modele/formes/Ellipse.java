package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.Remplissable;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * <p>
 *     Classe modélisant une ellipse.
 * </p>
 * <p>
 *     Hérite de la classe {@link Forme Forme}
 * </p>
 * <p>
 *     Une ellipse est coloriable. Elle implémente l'interface {@link Remplissable Remplissable}
 * </p>
 */
public class Ellipse extends Forme implements Remplissable {

    protected boolean rempli;

    public Ellipse(Coordonnees position, double largeur, double hauteur) {
        super(position, largeur, hauteur);
        if (largeur < 0 || hauteur < 0) {
            throw new IllegalArgumentException();
        }
        this.rempli = false;
    }

    public Ellipse(double largeur, double hauteur) {
        super(largeur, hauteur);
        if (largeur < 0 || hauteur < 0) {
            throw new IllegalArgumentException();
        }
    }

    public Ellipse(Coordonnees position) {
        super(position);
    }

    public Ellipse() {
        super();
    }

    @Override
    public boolean contient(Coordonnees point) {
        double a = this.getLargeur() / 2;
        double b = this.getHauteur() / 2;
        double cx = this.getCentre().getAbscisse();
        double cy = this.getCentre().getOrdonnee();
        double x = point.getAbscisse();
        double y = point.getOrdonnee();
        return (Math.pow(x - cx, 2) / Math.pow(a, 2)) + (Math.pow(y - cy, 2) / Math.pow(b, 2)) <= 1;
    }

    public Coordonnees getCentre() {
        double x = this.getPosition().getAbscisse() + (this.getLargeur() / 2);
        double y = this.getPosition().getOrdonnee() + (this.getHauteur() / 2);
        return new Coordonnees(x, y);
    }

    @Override
    public void setLargeur(double largeur) {
        if (largeur < 0) {
            throw new IllegalArgumentException();
        }
        super.setLargeur(largeur);
    }

    @Override
    public void setHauteur(double hauteur) {
        if (hauteur < 0) {
            throw new IllegalArgumentException();
        }
        super.setHauteur(hauteur);
    }

    @Override
    public double aire() {
        return Math.PI * (this.getLargeur() / 2) * (this.getHauteur() / 2);
    }

    @Override
    public double perimetre() {
        double a = this.getLargeur() / 2;
        double b = this.getHauteur() / 2;
        double h = Math.pow((a - b) / (a + b), 2);
        return Math.PI * (a + b) * (1 + (3 * h) / (10 + Math.sqrt(4 - 3 * h)));
    }

    @Override
    public String toString() {
        String pattern = "###0.0#";
        NumberFormat nf = NumberFormat.getNumberInstance();
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern(pattern);

        String stringRempli = (this.rempli) ? "-Rempli":"";

        return "[Ellipse" + stringRempli + "] : pos " + this.getPosition().toString() +
                " dim " + df.format(this.getLargeur()) + " x " + df.format(this.getHauteur()) +
                " périmètre : " + df.format(this.perimetre()) + " aire : " +
                df.format(this.aire()) + " " + this.toStringColor();
    }

    @Override
    public boolean estRempli() {
        return this.rempli;
    }

    @Override
    public void setRempli(boolean modeRemplissage) {
        this.rempli = modeRemplissage;
    }
}
