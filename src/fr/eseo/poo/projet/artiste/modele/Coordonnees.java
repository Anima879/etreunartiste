package fr.eseo.poo.projet.artiste.modele;


import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Objects;

/**
 * The Coordonnees class is used to represent a cartesian coordinate. The class offers the methods for<br>
 * <ul>
 * <li>Moving a coordinate
 * <li>Comparing two coordinates
 * <li>Calculating distance between two coordinates
 * <li>Calculating the angle between this coordinate and another coordinate
 * <li>Representing textually the coordinate.
 * </ul>
 *
 * @author Richard WOODWARD
 */
public class Coordonnees {

    public static final double EPSILON = 1e-2;

    /**
     * Class constant defining the default value for the abscissa of a Coordinate.
     */
    public static final double ABSCISSE_PAR_DEFAUT = 0.0;

    /**
     * Class constant defining the default value for the ardinate of a Coordinate.
     */
    public static final double ORDONNEE_PAR_DEFAUT = 0.0;

    private double abscisse;
    private double ordonnee;

    /**
     * Default Constructeur for Coordonnees. Creates a Coordonnees at the default coordinate as defined by
     * ({@link #ABSCISSE_PAR_DEFAUT},{@link #ORDONNEE_PAR_DEFAUT}).
     */
    public Coordonnees() {
        this(ABSCISSE_PAR_DEFAUT, ORDONNEE_PAR_DEFAUT);
    }

    /**
     * Creates a Coordonnnee at the given abscissa and ordinate.
     *
     * @param abscisse double representing the abscissa of the Coordonnees.
     * @param ordonnee double representing the ordinate of the Coordonnees.
     */
    public Coordonnees(double abscisse, double ordonnee) {
        this.abscisse = abscisse;
        this.ordonnee = ordonnee;
    }

    /**
     * Getter which recuperates the abscissa of the Coordonnees. This method should be generated automatically by Eclipse.
     *
     * @return double representing the abscissa of the Coordonnees.
     */
    public double getAbscisse() {
        return abscisse;
    }

    /**
     * Setter which modifies the absicssa of the Coordonnees. This method should be generated automatically by Eclipse.
     *
     * @param abscisse the new double value for the abscissa of the Coordonnees.
     */
    public void setAbscisse(double abscisse) {
        this.abscisse = abscisse;
    }

    /**
     * Getter which recuperates the ordinate of the Coordonnees. This method should be generated automatically by Eclipse.
     *
     * @return double representing the ordinate of the Coordonnees.
     */
    public double getOrdonnee() {
        return ordonnee;
    }

    /**
     * Setter which modifies the ordinate of the Coordonnees. This method should be generated automatically by Eclipse.
     *
     * @param ordonnee the new double value for the ordinate of the Coordonnees.
     */
    public void setOrdonnee(double ordonnee) {
        this.ordonnee = ordonnee;
    }

    /**
     * Method that moves this Coordonnees by a given movement vector.
     *
     * @param deltaX double representing the horizontal component of the movement vector.
     * @param deltaY  double representing the vertical component of the movement vector.
     */
    public void deplacerDe(double deltaX, double deltaY) {
        this.deplacerVers(this.abscisse + deltaX, this.ordonnee + deltaY);
    }

    /**
     * Method that moves this Coordonnees to a new position defined by the given abscissa and ordinate.
     *
     * @param nouvelleAbscisse double representing the new abscissa of this coordinate
     * @param nouvelleOrdonnee double representing the new ordinate of this coordinate
     */
    public void deplacerVers(double nouvelleAbscisse, double nouvelleOrdonnee) {
        this.abscisse = nouvelleAbscisse;
        this.ordonnee = nouvelleOrdonnee;
    }

    /**
     * Method that calculates the distance between this Coordonnees and another Coordonnees.
     *
     * @param autre the other Coordonnees.
     * @return double representing the distance between the two Coordonnees.
     */
    public double distanceVers(Coordonnees autre) {
        return Math.sqrt(Math.pow(autre.getAbscisse() - this.abscisse, 2)
                + Math.pow(autre.getOrdonnee() - this.ordonnee, 2));
    }

    /**
     *  Method that calculate the sum between this Coordonnees and another Coordonnees.
     *  Modify this Coordonnees.
     * @param other the other Coordonnees
     */
    public void add(Coordonnees other) {
        this.abscisse += other.getAbscisse();
        this.ordonnee += other.getOrdonnee();
    }

    /**
     * Method that calculates the angle (in radians) of the line which connects this Coordonnees with
     * another Coordonnees. The angle is measured between -pi (excluded) and pi (included) where zero radians is
     * equivalent to the other Coordonnees positioned horizontally to the right of this Coordonnees, with
     * no difference in the ordinates of the two Coordonnees.
     *
     * @param autre the other Coordonnees
     * @return double representing the angle (in radians) between this Coordonnees and another one.
     */
    public double angleVers(Coordonnees autre) {
        return Math.atan2( autre.getOrdonnee() - this.ordonnee, - this.abscisse + autre.getAbscisse());
    }

    @Override
    public String toString() {
        DecimalFormat df = (DecimalFormat)NumberFormat.getNumberInstance();
        df.applyPattern("###0.0#");
        return "(" + df.format(abscisse) + " , " + df.format(ordonnee) + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coordonnees)) return false;
        Coordonnees that = (Coordonnees) o;

        return Math.abs(that.getAbscisse() - getAbscisse()) <= EPSILON &&
                Math.abs(that.getOrdonnee() - getOrdonnee()) <= EPSILON;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAbscisse(), getOrdonnee());
    }
}