package fr.eseo.poo.projet.artiste.modele;

/**
 * <p>
 * Interface permettant d'implémenter la fonction de remplissage pour les formes pouvant être remplies.
 * </p>
 * <p>
 *     {@link fr.eseo.poo.projet.artiste.modele.formes.Forme formes} pouvant être remplies :
 * </p>
 * <ul>
 *     <li>{@link fr.eseo.poo.projet.artiste.modele.formes.Ellipse Ellipse}</li>
 *     <li>{@link fr.eseo.poo.projet.artiste.modele.formes.Cercle Cercle}</li>
 * </ul>
 */
public interface Remplissable {

    boolean estRempli();

    void setRempli(boolean modeRemplissage);
}
