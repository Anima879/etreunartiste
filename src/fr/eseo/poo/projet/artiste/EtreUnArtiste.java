package fr.eseo.poo.projet.artiste;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.JFrame;
import java.awt.BorderLayout;

/**
 * Copyright © 2020  Eloi Mahé
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Eloi Mahé
 * @version 1.0
 * @since 10-05-2020
 */
public class EtreUnArtiste {
    public static void main(String[] args) {
        JFrame mainWindow = new JFrame();
        mainWindow.setTitle("Etre un artiste");
        mainWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        PanneauDessin drawingPanel = new PanneauDessin();
        PanneauBarreOutils toolsPanel = new PanneauBarreOutils(drawingPanel);

        mainWindow.add(drawingPanel, BorderLayout.WEST);
        mainWindow.add(toolsPanel, BorderLayout.EAST);

        mainWindow.pack();
        mainWindow.setVisible(true);
    }
}
