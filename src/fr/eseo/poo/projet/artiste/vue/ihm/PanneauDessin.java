package fr.eseo.poo.projet.artiste.vue.ihm;

import fr.eseo.poo.projet.artiste.controleur.outils.Outil;
import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.vue.formes.VueForme;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Panneau de dessin</h1>
 * <p>
 * Panneau recevant et affichant les objets Forme.
 * </p>
 */
public class PanneauDessin extends JPanel implements ActionListener {

    /**
     * <h1>Constante d'instance</h1>
     * <p>
     * Liste des formes à afficher dans le panneau.
     * </p>
     *
     * @see ArrayList
     * @see List
     */
    private final List<VueForme> vueFormes = new ArrayList<>();

    /**
     * <h1>Constante de classe</h1>
     * <p>
     * Largeur de la zone de dessin par défaut.
     * </p>
     */
    public static final int LARGEUR_PAR_DEFAUT = 400;
    /**
     * <h1>Constante de classe</h1>
     * <p>
     * Hauteur de la zone de dessin par défaut.
     * </p>
     */
    public static final int HAUTEUR_PAR_DEFAUT = 240;
    /**
     * <h1>Constante de classe</h1>
     * <p>
     * Couleur de fond par défaut de la zone de dessin
     * </p>
     */
    public static final Color COULEUR_FOND_PAR_DEFAUT = Color.lightGray;

    /**
     * <p>
     *     Outil courant sélectionné pour agir sur le PanneauDessin.
     * </p>
     * @see Outil
     */
    private Outil outilCourant;
    /**
     * <p>
     *     Couleur courante pour dessiner les formes.
     *     (Cette couleur ne marche pas, elle court...)
     * </p>
     * @see Color
     */
    private Color couleurCourante;
    /**
     * <p>
     *     Mode de remplissage de la forme.
     *     N'est valable que pour {@link fr.eseo.poo.projet.artiste.modele.formes.Ellipse Ellipse} et
     *     {@link fr.eseo.poo.projet.artiste.modele.formes.Cercle Cercle}.
     * </p>
     * @see fr.eseo.poo.projet.artiste.modele.formes.Ellipse
     * @see fr.eseo.poo.projet.artiste.modele.formes.Cercle
     */
    private boolean modeRemplissage;

    /**
     * <h1>Constructeur exhaustif</h1>
     *
     * @param largeur de la zone de dessin
     * @param hauteur de la zone de dessin
     * @param fond    couleur (Objet {@link Color Color}) de fond de la zone de dessin.
     * @see Color
     */
    public PanneauDessin(int largeur, int hauteur, Color fond) {
        super();
        this.setPreferredSize(new Dimension(largeur, hauteur));
        this.setBackground(fond);
        this.outilCourant = null;
        this.couleurCourante = Forme.COULEUR_PAR_DEFAUT;
        this.modeRemplissage = false;
    }

    /**
     * <h1>Constructeur par défaut</h1>
     * <p>
     * Utilise les constantes de classe pas défaut :
     * </p>
     *
     * <ul>
     * <li>LARGEUR_PAR_DEFAUT {@value LARGEUR_PAR_DEFAUT}</li>
     * <li>HAUTEUR_PAR_DEFAUT {@value HAUTEUR_PAR_DEFAUT}</li>
     * <li>COULEUR_FOND_PAR_DEFAUT</li>
     * </ul>
     */
    public PanneauDessin() {
        this(LARGEUR_PAR_DEFAUT, HAUTEUR_PAR_DEFAUT, COULEUR_FOND_PAR_DEFAUT);
    }

    public void ajouterVueForme(VueForme forme) {
        this.vueFormes.add(forme);
    }

    public List<VueForme> getVueFormes() {
        return vueFormes;
    }


    /**
     * <p>
     * Associe un outil donné en paramètre à cet instance de PanneauDessin et  l'ajoute au gestionnaire d'événement.
     * Si l'outil donné en paramètre est déjà associé au panneau, l'association est ignorée.
     * S'il est différent, la méthode dissocie le précédent outil grâce à la
     * méthode {@link #dissocierOutil() dissocierOutil}.
     * </p>
     *
     * @param outil instance héritant de la classe abstraite Outil
     * @see Outil
     */
    public void associerOutil(Outil outil) {
        if (this.getOutilCourant() != outil) {
            this.dissocierOutil();
            this.setOutilCourant(outil);
            outil.setPanneauDessin(this);
            addMouseListener(outil);
            addMouseMotionListener(outil);
        }
    }

    /**
     * Dissocie l'outil associé avec {@link #associerOutil(Outil) associerOutil} et le retire du
     * gestionnaire d'événement.
     * Si aucun outil n'est associé, la méthode est ignorée.
     * @see Outil
     */
    private void dissocierOutil() {
        if (this.getOutilCourant() != null) {
            removeMouseListener(this.outilCourant);
            removeMouseMotionListener(this.outilCourant);
            this.outilCourant.setPanneauDessin(null);
            this.outilCourant = null;
        }
    }

    // Getters and setters

    public Outil getOutilCourant() {
        return outilCourant;
    }

    private void setOutilCourant(Outil outilCourant) {
        this.outilCourant = outilCourant;
    }

    public Color getCouleurCourante() {
        return couleurCourante;
    }

    public void setCouleurCourante(Color couleurCourante) {
        this.couleurCourante = couleurCourante;
    }

    public boolean getModeRemplissage() {
        return modeRemplissage;
    }

    public void setModeRemplissage(boolean modeRemplissage) {
        this.modeRemplissage = modeRemplissage;
    }

    // Override methods

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g.create();
        for (VueForme f : vueFormes) {
            f.affiche(g2d);
        }

        g2d.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
