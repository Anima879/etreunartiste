package fr.eseo.poo.projet.artiste.vue.ihm;

import fr.eseo.poo.projet.artiste.controleur.actions.*;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JCheckBox;
import javax.swing.ButtonGroup;
import javax.swing.JSpinner;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.JLabel;


/**
 * {@link JPanel} gérant les outils utiles au dessin.
 */
public class PanneauBarreOutils extends JPanel {

    private PanneauDessin panneauDessin;

    public static final String BRANCHE_SPINNER_NOM = "Nombre de branche";
    public static final String LONGUEUR_SPINNER_NOM = "Proportion des branches";

    private SpinnerModel modelNbrBranche;
    private SpinnerModel modelTailleBranche;

    public PanneauBarreOutils(PanneauDessin panneauDessin) {
        this.panneauDessin = panneauDessin;
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.initComponents();
    }

    public PanneauDessin getPanneauDessin() {
        return panneauDessin;
    }

    public void setPanneauDessin(PanneauDessin panneauDessin) {
        this.panneauDessin = panneauDessin;
    }

    public void initComponents() {
        // Création du bouton "Tout effacer"
        JButton eraseAllButton = new JButton(new ActionEffacer(panneauDessin));
        eraseAllButton.setName(ActionEffacer.NOM_ACTION);
        this.add(eraseAllButton);

        // Création du bouton pour l'outil Ligne
        JToggleButton ligneButton = new JToggleButton(
                new ActionChoisirForme(panneauDessin, this, "Ligne"));
        this.add(ligneButton);
        ligneButton.setText(ActionChoisirForme.NOM_ACTION_LIGNE);
        ligneButton.setName(ActionChoisirForme.NOM_ACTION_LIGNE);

        // Création du bouton pour l'outil Rectangle
        JToggleButton rectButton = new JToggleButton(
                new ActionChoisirForme(panneauDessin, this, "Rectangle"));
        this.add(rectButton);
        rectButton.setText(ActionChoisirForme.NOM_ACTION_RECTANGLE);
        rectButton.setName(ActionChoisirForme.NOM_ACTION_RECTANGLE);

        // Création du bouton pour l'outil Ellipse
        JToggleButton ellipseButton = new JToggleButton(
                new ActionChoisirForme(panneauDessin, this, "Ellipse"));
        ellipseButton.setText(ActionChoisirForme.NOM_ACTION_ELLIPSE);
        ellipseButton.setName(ActionChoisirForme.NOM_ACTION_ELLIPSE);
        this.add(ellipseButton);

        // Création du bouton pour l'outil Cercle
        JToggleButton cercleButton = new JToggleButton(
                new ActionChoisirForme(panneauDessin, this, "Cercle"));
        cercleButton.setText(ActionChoisirForme.NOM_ACTION_CERCLE);
        cercleButton.setName(ActionChoisirForme.NOM_ACTION_CERCLE);
        this.add(cercleButton);

        // Création du bouton Etoile
        JToggleButton etoileButton = new JToggleButton(
                new ActionChoisirForme(panneauDessin, this,"Etoile"));
        etoileButton.setText(ActionChoisirForme.NOM_ACTION_ETOILE);
        etoileButton.setName(ActionChoisirForme.NOM_ACTION_ETOILE);
        this.add(etoileButton);

        // Création du bouton pour l'outil Effacer
        JToggleButton eraseButton = new JToggleButton(new ActionSimpleEffacer(panneauDessin));
        eraseButton.setName(ActionSimpleEffacer.NOM_ACTION);
        eraseButton.setText(ActionSimpleEffacer.NOM_ACTION);
        this.add(eraseButton);

        // Création du bouton pour l'outil Selectionner
        JToggleButton selectButton = new JToggleButton(
                new ActionSelectionner(panneauDessin));
        selectButton.setText(ActionSelectionner.NOM_ACTION);
        selectButton.setName(ActionSelectionner.NOM_ACTION);
        this.add(selectButton);

        // Création du groupe de boutons et ajout des JToggleButton
        ButtonGroup tools = new ButtonGroup();
        tools.add(ligneButton);
        tools.add(ellipseButton);
        tools.add(cercleButton);
        tools.add(selectButton);
        tools.add(etoileButton);
        tools.add(eraseButton);
        tools.add(rectButton);

        // Création du bouton du choix des couleurs
        JButton colorButton = new JButton(new ActionChoisirCouleur(panneauDessin));
        colorButton.setName(ActionChoisirCouleur.NOM_ACTION);
        colorButton.setText(ActionChoisirCouleur.NOM_ACTION);
        this.add(colorButton);

        // Création de la case à cocher du mode de remplissage.
        JCheckBox fillBox = new JCheckBox(new ActionChoisirRemplissage(panneauDessin));
        fillBox.setSelected(panneauDessin.getModeRemplissage());
        fillBox.setName(ActionChoisirRemplissage.NOM_ACTION);
        fillBox.setText(ActionChoisirRemplissage.NOM_ACTION);
        this.add(fillBox);

        // Création de la sélection du nombre de branche de l'étoile
        JLabel texteNbrBranche = new JLabel(BRANCHE_SPINNER_NOM);
        this.modelNbrBranche = new SpinnerNumberModel(Etoile.NOMBRE_BRANCHES_PAR_DEFAUT, 3, 15, 1);
        JSpinner nbrBranche = new JSpinner(modelNbrBranche);
        nbrBranche.setName(BRANCHE_SPINNER_NOM);
        this.add(texteNbrBranche);
        this.add(nbrBranche);

        // Création de la sélection de la taille des branches de l'étoile
        JLabel texteTailleBranche = new JLabel(LONGUEUR_SPINNER_NOM);
        this.modelTailleBranche = new SpinnerNumberModel(Etoile.LONGUEUR_BRANCHE_PAR_DEFAUT,
                0.0, 1.0, 0.01);
        JSpinner tailleBranche = new JSpinner(modelTailleBranche);
        tailleBranche.setName(LONGUEUR_SPINNER_NOM);
        this.add(texteTailleBranche);
        this.add(tailleBranche);

        // Création du bouton enregistrer
        JButton saveButton = new JButton(new ActionEnregistrer(panneauDessin));
        saveButton.setName(ActionEnregistrer.NOM_ACTION);
        saveButton.setText(ActionEnregistrer.NOM_ACTION);
        this.add(saveButton);

        // Création du bouton ouvrir
        JButton openButton = new JButton(new ActionOuvrir(panneauDessin));
        openButton.setName(ActionOuvrir.NOM_ACTION);
        openButton.setText(ActionOuvrir.NOM_ACTION);
        this.add(openButton);

    }

    public int getNbBranches() {
        return (int) this.modelNbrBranche.getValue();
    }

    public double getLongueurBranche() {
        return (double) this.modelTailleBranche.getValue();
    }
}
