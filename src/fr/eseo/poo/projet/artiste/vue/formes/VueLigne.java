package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.formes.Ligne;

import java.awt.*;


/**
 * Classe se chargeant de l'affiche de l'objet {@link Ligne} avec le contexte graphique
 * {@link Graphics2D}.
 */
public class VueLigne extends VueForme {

    public VueLigne(Ligne ligne) {
        super(ligne);
    }

    @Override
    public void affiche(Graphics2D g2d) {
        int x = (int) Math.round(this.getForme().getPosition().getAbscisse());
        int y = (int) Math.round(this.getForme().getPosition().getOrdonnee());
        int largeur = (int) Math.round(this.getForme().getLargeur());
        int hauteur = (int) Math.round(this.getForme().getHauteur());

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        // Sauvegarde de la couleur courante
        Color save = g2d.getColor();
        // On modifie la couleur de g2d avec la couleur de la forme
        g2d.setColor(this.getForme().getCouleur());
        g2d.drawLine(x, y, x + largeur, y + hauteur);
        // On restaure la couleur de base sauvegardé.
        g2d.setColor(save);
    }
}
