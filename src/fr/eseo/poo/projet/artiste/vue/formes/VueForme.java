package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.formes.Forme;

import java.awt.Graphics2D;

/**
 * Classe abstraite se chargeant de l'affiche d'une {@link Forme} avec le contexte
 * graphique {@link Graphics2D}.
 */
public abstract class VueForme {

    protected final Forme forme;
    protected boolean ghost;

    public VueForme(Forme forme) {
        this.forme = forme;
    }

    public Forme getForme() {
        return forme;
    }

    public void setGhost(boolean bool) {
        this.ghost = bool;
    }

    public boolean isGhost() {
        return this.ghost;
    }

    /**
     * Méthode chargée de l'affichage.
     * @param g2d : objet graphique servant à dessiner.
     * @see Graphics2D
     */
    public abstract void affiche(Graphics2D g2d);
}
