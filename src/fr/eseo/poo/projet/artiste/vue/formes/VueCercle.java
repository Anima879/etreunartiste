package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.formes.Cercle;


/**
 * Classe se chargeant de l'affiche de l'objet {@link Cercle} avec le contexte graphique
 * hérité de {@link fr.eseo.poo.projet.artiste.modele.formes.Ellipse Ellipse}.
 */
public class VueCercle extends VueEllipse {
    public VueCercle(Cercle forme) {
        super(forme);
    }
}
