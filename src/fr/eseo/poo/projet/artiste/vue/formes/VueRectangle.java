package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.formes.Forme;
import fr.eseo.poo.projet.artiste.modele.formes.Rectangle;

import java.awt.*;

public class VueRectangle extends VueForme {

    private boolean fromSelection;

    public VueRectangle(Forme forme) {
        super(forme);
    }

    @Override
    public Rectangle getForme() {
        return (Rectangle) super.getForme();
    }

    @Override
    public void affiche(Graphics2D g2d) {
        Color save = g2d.getColor();
        g2d.setColor(this.getForme().getCouleur());

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        if (this.getForme().estRempli())
            g2d.fillRect((int) this.getForme().getPosition().getAbscisse(),
                    (int) this.getForme().getPosition().getOrdonnee(),
                    (int) this.getForme().getLargeur(), (int) this.getForme().getHauteur());

        g2d.drawRect((int) this.getForme().getPosition().getAbscisse(),
                (int) this.getForme().getPosition().getOrdonnee(),
                (int) this.getForme().getLargeur(), (int) this.getForme().getHauteur());
        g2d.setColor(save);
    }
    
    public boolean isFromSelection() {
        return fromSelection;
    }

    public void setFromSelection(boolean fromSelection) {
        this.fromSelection = fromSelection;
    }
}
