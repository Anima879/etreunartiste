package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.formes.Etoile;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;

/**
 * Classe modélisant la forme graphique d'une étoile.
 *
 * @author Eloi Mahé
 * @version 1.0
 * @since 01/05/2020
 */
public class VueEtoile extends VueForme {

    public VueEtoile(Etoile forme) {
        super(forme);
    }

    @Override
    public Etoile getForme() {
        return (Etoile) super.getForme();
    }

    @Override
    public void affiche(Graphics2D g2d) {
        Color save = g2d.getColor();
        g2d.setColor(this.getForme().getCouleur());
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        GeneralPath etoile = new GeneralPath(GeneralPath.WIND_NON_ZERO, this.getForme().getCoordonnees().size());
        double x = this.getForme().getCoordonnees().get(0).getAbscisse();
        double y = this.getForme().getCoordonnees().get(0).getOrdonnee();

        etoile.moveTo(Math.round(x), Math.round(y));
        for (int i = 1; i < this.getForme().getCoordonnees().size(); i++) {
            x = this.getForme().getCoordonnees().get(i).getAbscisse();
            y = this.getForme().getCoordonnees().get(i).getOrdonnee();
            etoile.lineTo(Math.round(x), Math.round(y));
        }
        etoile.closePath();

        if (this.getForme().estRempli())
            g2d.fill(etoile);

        g2d.draw(etoile);
        g2d.setColor(save);
    }
}
