package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 * Classe se chargeant de l'affiche de l'objet {@link Ellipse} avec le contexte graphique
 * {@link Graphics2D}.
 */
public class VueEllipse extends VueForme {

    public VueEllipse(Ellipse forme) {
        super(forme);
    }

    @Override
    public Ellipse getForme() {
        return (Ellipse) super.getForme();
    }

    @Override
    public void affiche(Graphics2D g2d) {
        int x = (int) Math.round(this.getForme().getPosition().getAbscisse());
        int y = (int) Math.round(this.getForme().getPosition().getOrdonnee());
        int largeur = (int) Math.round(this.getForme().getLargeur());
        int hauteur = (int) Math.round(this.getForme().getHauteur());

        // Sauvegarde de la couleur courante
        Color save = g2d.getColor();
        // On modifie la couleur de g2d avec la couleur de la forme
        g2d.setColor(this.getForme().getCouleur());
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        if (this.getForme().estRempli())
            g2d.fillOval(x, y, largeur, hauteur);

        g2d.drawOval(x, y, largeur, hauteur);
        // On restaure la couleur de base sauvegardé.
        g2d.setColor(save);
    }
}
