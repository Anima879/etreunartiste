<h1>Etre un artiste</h1>
<p>
L’objectif de ce bureau d’études est de développer un éditeur graphique offrant des fonctionnalités similaires à
celles fournies par des logiciels bien connus de tous (eg Dia, Inkscape ou MSPaint).
</p>
<p>
L’éditeur que nous concevrons dans le cadre de ce travail permettra de dessiner les formes géométriques telles
que les lignes, les ellipses, les cercles et les étoiles.
</p>
<p>Projet développé sous IntelliJ.</p>

<h2>Extensions</h2>
<ul>
<li>Anticrénelage</li>
<li>Effacer une forme à la fois</li>
<li>Forme Rectangle</li>
<li>Les formes sélectionnées sont encadrées</li>
<li>Prévisualisation des formes en construction</li>
</ul>

EtreUnArtiste
Copyright © 2020  Eloi Mahé

This program is free software: you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published by the 
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
