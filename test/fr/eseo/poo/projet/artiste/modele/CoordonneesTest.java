package fr.eseo.poo.projet.artiste.modele;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class CoordonneesTest {

    public static final double DELTA = 1e-3;

    @Test
    public void testDefaultConstructor() {
        Coordonnees test = new Coordonnees();
        assertEquals(0.0, test.getAbscisse(), 0);
        assertEquals(0.0, test.getOrdonnee(), 0);
    }

    @Test
    public void testParamConstructor() {
        Coordonnees test = new Coordonnees(2.3, 4.87);
        assertEquals(2.3, test.getAbscisse(), 0);
        assertEquals(4.87, test.getOrdonnee(), 0);
    }

    @Test
    public void testParamConstructorInt() {
        Coordonnees test = new Coordonnees(2, 8);
        assertEquals(2, test.getAbscisse(), 0);
        assertEquals(8, test.getOrdonnee(), 0);
    }

    @Test
    public void testSetAbscisse() {
        Coordonnees test = new Coordonnees();
        test.setAbscisse(5);
        assertEquals("Mutateur abscisse", 5, test.getAbscisse(), DELTA);
    }

    @Test
    public void testSetOrdonnee() {
        Coordonnees test = new Coordonnees();
        test.setOrdonnee(5);
        assertEquals("Mutateur abscisse", 5, test.getOrdonnee(), DELTA);
    }

    @Test
    public void testToString() {
        Coordonnees test = new Coordonnees(3.14, 3.14);
        assertEquals("Bon format", "(3,14 , 3,14)", test.toString());
    }

    @Test
    public void testToStringWithZero() {
        Coordonnees test = new Coordonnees(3, 3);
        assertEquals("Bon format", "(3,0 , 3,0)", test.toString());
    }

    @Test
    public void testEquals() {
        Coordonnees a = new Coordonnees(3.14, 16.2);
        Coordonnees b = new Coordonnees(3.14, 16.2);
        assertEquals("Les coordonnées doivent être égales", a, b);
        b = new Coordonnees(0, 5);
        assertNotEquals("Les coordonnées ne doivent pas être égales", a, b);
    }

    @Test
    public void testAdd() {
        Coordonnees a = new Coordonnees(5, 5);
        Coordonnees b = new Coordonnees(1, 7);
        a.add(b);
        assertEquals("Somme des coordonnées", new Coordonnees(6, 12), a);
    }

    @Test
    public void testHashCode() {
        Coordonnees a = new Coordonnees(3.14, 3.14);
        Coordonnees b = new Coordonnees(3.14, 3.14);
        assertEquals("Les hashcode doivent être identiques", a.hashCode(), b.hashCode());
        b = new Coordonnees(5, 2);
        assertNotEquals("Les hashcode ne doivent pas être identiques", a.hashCode(), b.hashCode());
    }
}