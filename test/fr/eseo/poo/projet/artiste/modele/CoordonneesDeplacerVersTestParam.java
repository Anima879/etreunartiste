package fr.eseo.poo.projet.artiste.modele;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CoordonneesDeplacerVersTestParam {

    private static final double DELTA = 1e-3;

    private final double x, y;
    private final double destX, destY;

    public CoordonneesDeplacerVersTestParam(double x, double y, double destX, double destY) {
        this.x = x;
        this.y = y;
        this.destX = destX;
        this.destY = destY;
    }

    @Parameters(name = "dt[{index}] : ({0}, {1}), ({2}, {3})")
    public static Collection<Object[]> dt() {
        Object[][] data = new Object[][]{
                {0, 0, 4, 4},
                {0, 0, -4, -4},
                {3.2, 7.25, 0.2, -7.1},
                {0, 0, -2.35, 7.21},
        };

        return Arrays.asList(data);
    }

    @Test
    public void deplacerVers() {
        Coordonnees test = new Coordonnees(x, y);
        test.deplacerVers(destX, destY);
        assertEquals(destX, test.getAbscisse(), DELTA);
        assertEquals(destY, test.getOrdonnee(), DELTA);
    }
}