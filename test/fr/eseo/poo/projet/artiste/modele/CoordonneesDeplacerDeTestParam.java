package fr.eseo.poo.projet.artiste.modele;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CoordonneesDeplacerDeTestParam {

    private static final double DELTA = 1e-3;

    private final double abs, ord;
    private final double dx, dy;
    private final double resultAbs, resultOrd;

    /**
     * Constructeur de chaque donnée Coordonnees de test.
     *
     * @param abs       Abscisse à tester
     * @param ord       Ordonnée à tester.
     * @param resultAbs Abscisse attendue
     * @param resultOrd Ordonnée attendu.
     * @see Coordonnees
     */
    public CoordonneesDeplacerDeTestParam(double abs, double ord, double dx, double dy, double resultAbs, double resultOrd) {
        this.abs = abs;
        this.ord = ord;
        this.dx = dx;
        this.dy = dy;
        this.resultAbs = resultAbs;
        this.resultOrd = resultOrd;
    }

    @Parameters(name = "dt[{index}] : ({0}, {1}), ({2}, {3}), ({4}, {5})")
    public static Collection<Object[]> dt() {
        Object[][] data = new Object[][]{
                {0, 0, 2, 0, 2, 0},
                {0, 0, 0, 2, 0, 2},
                {2.5, 2.5, 0.5, 0.5, 3, 3},
                {6.8, 3.2, -2.3, -1.85, 4.5, 1.35},
                {7, -5.2, -3, 2.1, 4, -3.1},
                {-4.2, -4.2, -4.2, -4.2, -8.4, -8.4},
                {6.8, 3.2, -2.3, 1.85, 4.5, 5.05}
        };

        return Arrays.asList(data);
    }

    @Test
    public void deplacerDe() {
        Coordonnees test = new Coordonnees(abs, ord);
        test.deplacerDe(dx, dy);
        assertEquals(resultAbs, test.getAbscisse(), DELTA);
        assertEquals(resultOrd, test.getOrdonnee(), DELTA);
    }
}