package fr.eseo.poo.projet.artiste.modele.formes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import org.junit.Test;

import javax.swing.UIManager;
import java.awt.Color;

public class LigneTest {

    private static final double DELTA = 1e-3;

    @Test
    public void testDefaultConstructor() {
        Ligne test = new Ligne();
        assertEquals("Position C1 par défaut", new Coordonnees(), test.getC1());
        Coordonnees point = new Coordonnees();
        point.deplacerDe(Forme.LARGEUR_PAR_DEFAUT, Forme.HAUTEUR_PAR_DEFAUT);
        assertEquals("Position C2 par défaut", point, test.getC2());
    }

    @Test
    public void testConstructor() {
        Ligne test = new Ligne(3.14, 3.14);
        assertEquals("Position C1", new Coordonnees(), test.getC1());
        assertEquals("Position C2 par défaut", new Coordonnees(3.14, 3.14), test.getC2());
    }

    @Test
    public void testConstructorPosition() {
        Ligne test = new Ligne(new Coordonnees(3.14, 3.14));
        assertEquals("Position de C1", new Coordonnees(3.14, 3.14), test.getC1());
        assertEquals("Largeur par défaut", Forme.LARGEUR_PAR_DEFAUT, test.getLargeur(), DELTA);
        assertEquals("Hauteur par défaut", Forme.HAUTEUR_PAR_DEFAUT, test.getHauteur(), DELTA);
        Coordonnees pointC2 = new Coordonnees(3.14 + Forme.LARGEUR_PAR_DEFAUT,
                3.14 + Forme.HAUTEUR_PAR_DEFAUT);
        assertEquals("Point C2", pointC2, test.getC2());
    }

    @Test
    public void testSetC2() {
        Ligne test = new Ligne();
        Coordonnees point = new Coordonnees(3.14, 3.14);
        test.setC2(point);
        assertEquals("Déplacement du point C2", point, test.getC2());
    }

    @Test
    public void testGetCadreMin() {
        Ligne test = new Ligne(new Coordonnees(3.14, 3.14), 16.2, -1.36);
        assertEquals("Abscisse minimal du cadre", 3.14, test.getCadreMinX(), DELTA);
        assertEquals("Ordonnée minimal du cadre", 1.78, test.getCadreMinY(), DELTA);
    }

    @Test
    public void testGetCadreMax() {
        Ligne test = new Ligne(new Coordonnees(20, 15), -3, 6);
        assertEquals("Abscisse maximal du cadre", 20, test.getCadreMaxX(), DELTA);
        assertEquals("Ordonnée maximal du cadre", 21, test.getCadreMaxY(), DELTA);
    }

    @Test
    public void testAire() {
        Ligne test = new Ligne();
        assertEquals("Aire nulle", 0, test.aire(), DELTA);
    }

    @Test
    public void testPerimetre() {
        Ligne test = new Ligne(3.14, 3.14);
        assertEquals("Longueur de la ligne", 4.44, test.perimetre(), DELTA);
    }

    @Test
    public void testToString() {
        Coordonnees point = new Coordonnees(1.62, 1.62);
        Ligne test = new Ligne(point, 3.14, 3.14);
        String text = "[Ligne] c1 : (1,62 , 1,62) c2 : (4,76 , 4,76) longueur : 4,44 angle : 45,0° couleur = R51,V51,B51";
        assertEquals("Affichage textuel", text, test.toString());
    }

    @Test
    public void testSetC1() {
        Ligne test = new Ligne();
        test.setC1(new Coordonnees(5, 5));
        assertEquals("Nouveau point", new Coordonnees(5, 5), test.getC1());
        assertEquals("Largeur modifiée", 5, test.getLargeur(), DELTA);
        assertEquals("Hauteur modifiée", 5, test.getHauteur(), DELTA);
    }

    @Test
    public void testContient() {
        Ligne test = new Ligne();
        assertTrue("Contient", test.contient(new Coordonnees(3.14, 3.14)));
        assertFalse("Ne contient pas", test.contient(new Coordonnees(3.14, 1.62)));
    }

    @Test
    public void testGetCouleur() {
        Ligne test = new Ligne();
        assertEquals("Couleur par défaut", UIManager.getColor("Panel.foreground"), test.getCouleur());
    }

    @Test
    public void testSetCouleur() {
        Ligne test = new Ligne();
        test.setCouleur(Color.GREEN);
        assertEquals("Mutateur de la couleur", Color.GREEN, test.getCouleur());
    }

    @Test
    public void testDeplacerVersCoord() {
        Ligne test = new Ligne();
        test.deplacerVers(new Coordonnees(5, 5));
        assertEquals("Deplacement de la forme", new Coordonnees(5, 5), test.getPosition());
        test.deplacerVers(new Coordonnees(-5, 5));
        assertEquals("Deplacement de la forme", new Coordonnees(-5, 5), test.getPosition());
        test.deplacerVers(new Coordonnees(5, -5));
        assertEquals("Deplacement de la forme", new Coordonnees(5, -5), test.getPosition());
        test.deplacerVers(new Coordonnees(-5, -5));
        assertEquals("Deplacement de la forme", new Coordonnees(-5, -5), test.getPosition());
    }

    @Test
    public void testDeplacerVersAbsOrd() {
        Ligne test = new Ligne();
        test.deplacerVers(5, 5);
        assertEquals("Deplacement de la forme", new Coordonnees(5, 5), test.getPosition());
        test.deplacerVers(-5, 5);
        assertEquals("Deplacement de la forme", new Coordonnees(-5, 5), test.getPosition());
        test.deplacerVers(5, -5);
        assertEquals("Deplacement de la forme", new Coordonnees(5, -5), test.getPosition());
        test.deplacerVers(-5, -5);
        assertEquals("Deplacement de la forme", new Coordonnees(-5, -5), test.getPosition());
    }

    @Test
    public void testDeplacerDe() {
        Ligne test = new Ligne(new Coordonnees(3.14, 3.14));
        test.deplacerDe(2, 2);
        assertEquals("Deplacement de la forme selon un vecteur", new Coordonnees(5.14, 5.14),
                test.getPosition());
        test.deplacerDe(-2, 2);
        assertEquals("Deplacement de la forme selon un vecteur", new Coordonnees(3.14, 7.14),
                test.getPosition());
        test.deplacerDe(2, -2);
        assertEquals("Deplacement de la forme selon un vecteur", new Coordonnees(5.14, 5.14),
                test.getPosition());
        test.deplacerDe(-2, -2);
        assertEquals("Deplacement de la forme selon un vecteur", new Coordonnees(3.14, 3.14),
                test.getPosition());
    }

//    @Test
//    public void testEquals() {
//        Ligne a = new Ligne();
//        Ligne b = new Ligne();
//        assertEquals("Les deux lignes doivent être égales", a, b);
//    }
}
