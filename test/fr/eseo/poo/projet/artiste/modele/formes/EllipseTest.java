package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.swing.UIManager;
import java.awt.Color;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class EllipseTest {

    private static final double DELTA = 1e-3;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testDefaultConstructor() {
        Ellipse test = new Ellipse();
        assertEquals("Position par défaut", new Coordonnees(), test.getPosition());
        assertEquals("Largeur par défaut", Ellipse.LARGEUR_PAR_DEFAUT, test.getLargeur(), DELTA);
        assertEquals("Largeur par défaut", Ellipse.HAUTEUR_PAR_DEFAUT, test.getHauteur(), DELTA);
        assertFalse("Remplissage par défaut", test.estRempli());
    }

    @Test
    public void testConstructorPosition() {
        Ellipse test = new Ellipse(new Coordonnees(3.14, 1.62));
        assertEquals("Position", new Coordonnees(3.14, 1.62), test.getPosition());
        assertEquals("Largeur par défaut", Ellipse.LARGEUR_PAR_DEFAUT, test.getLargeur(), DELTA);
        assertEquals("Hauteur par défaut", Ellipse.HAUTEUR_PAR_DEFAUT, test.getHauteur(), DELTA);
    }

    @Test
    public void testAire() {
        Ellipse test = new Ellipse(3.14, 1.62);
        assertEquals("Aire : ", Math.PI * (3.14 / 2) * (1.62 / 2), test.aire(), DELTA);
    }

    @Test
    public void testPerimetre() {
        Ellipse test = new Ellipse(3.14, 1.62);
        double a = (3.14 / 2);
        double b = (1.62 / 2);
        double h = Math.pow(((a - b) / (a + b)), 2);
        double perimetre = Math.PI * (a + b) * (1 + (3 * h) / (10 + Math.sqrt(4 - 3 * h)));
        assertEquals("Périmètre : ", perimetre, test.perimetre(), DELTA);
    }

    @Test
    public void testGetCentre() {
        Ellipse test = new Ellipse(new Coordonnees(-10, -5), 20, 10);
        Coordonnees centre = new Coordonnees();
        assertEquals("Centre à l'origine", centre, test.getCentre());
        test = new Ellipse(new Coordonnees(3.14, 3.14), 20, 10);
        centre = new Coordonnees(13.14, 8.14);
        assertEquals("Centre à (13.14, 8.14)", centre, test.getCentre());
    }

    @Test
    public void testContient() {
        Ellipse test = new Ellipse(20, 10);
        Coordonnees a = new Coordonnees();
        Coordonnees b = new Coordonnees(18, 6);
        assertTrue("Est contenu", test.contient(b));
        assertFalse("N'est pas contenu", test.contient(a));
    }

    @Test
    public void testExceptionConstructor() {
        expectedException.expect(IllegalArgumentException.class);
        new Ellipse(new Coordonnees(3.14, 1.62), -5, 10);
        new Ellipse(new Coordonnees(3.14, 1.62), 5, -10);
    }

    @Test
    public void testSetHauteurException() {
        Ellipse test = new Ellipse();
        expectedException.expect(IllegalArgumentException.class);
        test.setHauteur(-10);
    }

    @Test
    public void testSetLargeurException() {
        Ellipse test = new Ellipse();
        expectedException.expect(IllegalArgumentException.class);
        test.setLargeur(-10);
    }

    @Test
    public void testToString() {
        Ellipse test = new Ellipse(new Coordonnees(3.14, 3.14), 10, 20);
        String text = "[Ellipse] : pos (3,14 , 3,14) dim 10,0 x 20,0 périmètre : 48,44 aire : 157,08 " +
                "couleur = R51,V51,B51";
        assertEquals("Le texte est incorrect", text, test.toString());
        test.setRempli(true);
        text = "[Ellipse-Rempli] : pos (3,14 , 3,14) dim 10,0 x 20,0 périmètre : 48,44 aire : 157,08 " +
                "couleur = R51,V51,B51";
        assertEquals("Le texte est incorrect", text, test.toString());
    }

    @Test
    public void testRemplissage() {
        Ellipse test = new Ellipse();
        assertFalse("La forme n'est pas remplie", test.estRempli());
        test.setRempli(true);
        assertTrue("La forme est remplie", test.estRempli());
    }

    @Test
    public void testGetCouleur() {
        Ellipse test = new Ellipse();
        assertEquals("Couleur par défaut", UIManager.getColor("Panel.foreground"), test.getCouleur());
    }

    @Test
    public void testSetCouleur() {
        Ellipse test = new Ellipse();
        test.setCouleur(Color.GREEN);
        assertEquals("Mutateur de la couleur", Color.GREEN, test.getCouleur());
    }
}
