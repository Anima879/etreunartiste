package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RectangleTest {

    public static final double EPSILON = 1e-3;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testConstructorException() {
        expectedException.expect(IllegalArgumentException.class);
        new Rectangle(-5, -5);
    }

    @Test
    public void aire() {
        Rectangle test = new Rectangle();
        assertEquals("Aire", 100, test.aire(), EPSILON);
    }

    @Test
    public void perimetre() {
        Rectangle test = new Rectangle();
        assertEquals("Périmètre", 40, test.perimetre(), EPSILON);
    }

    @Test
    public void contient() {
        Rectangle test = new Rectangle();
        Coordonnees point1 = new Coordonnees();
        Coordonnees point2 = new Coordonnees(5, 5);
        Coordonnees point3 = new Coordonnees(15, 10);
        Coordonnees point4 = new Coordonnees(10, 15);
        Coordonnees point5 = new Coordonnees(15, 15);
        assertTrue("Est contenu", test.contient(point1));
        assertTrue("Est contenu", test.contient(point2));
        assertFalse("N'est pas contenu", test.contient(point3));
        assertFalse("N'est pas contenu", test.contient(point4));
        assertFalse("N'est pas contenu", test.contient(point5));
    }
}