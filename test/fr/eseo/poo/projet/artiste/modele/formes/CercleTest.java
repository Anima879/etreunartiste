package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.swing.UIManager;
import java.awt.Color;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class CercleTest {

    private static final double DELTA = 1e-3;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testDefaultConstructor() {
        Cercle test = new Cercle();
        assertEquals("Position par défaut", new Coordonnees(), test.getPosition());
        assertEquals("Largeur par défaut", Ellipse.LARGEUR_PAR_DEFAUT, test.getLargeur(), DELTA);
        assertEquals("Largeur par défaut", Ellipse.HAUTEUR_PAR_DEFAUT, test.getHauteur(), DELTA);
    }

    @Test
    public void testConstructor() {
        Cercle test = new Cercle(new Coordonnees(3.14, 3.14), 10);
        assertEquals("Position", new Coordonnees(3.14, 3.14), test.getPosition());
        assertEquals("Largeur", 10, test.getLargeur(), DELTA);
        assertEquals("Hauteur", 10, test.getHauteur(), DELTA);
    }

    @Test
    public void testCosntructorPosition() {
        Cercle test = new Cercle(new Coordonnees(3.14, 1.62));
        assertEquals("Position", new Coordonnees(3.14, 1.62), test.getPosition());
        assertEquals("Largeur par défaut", Cercle.LARGEUR_PAR_DEFAUT, test.getLargeur(), DELTA);
        assertEquals("Hauteur par défaut", Cercle.HAUTEUR_PAR_DEFAUT, test.getHauteur(), DELTA);
    }

    @Test
    public void testAire() {
        Cercle test = new Cercle(3.14);
        assertEquals("Aire : ", Math.PI * Math.pow((3.14 / 2), 2), test.aire(), DELTA);
    }

    @Test
    public void testPerimetre() {
        Cercle test = new Cercle(3.14);
        double perimetre = Math.PI * 2 * (3.14 / 2);
        assertEquals("Périmètre : ", perimetre, test.perimetre(), DELTA);
    }

    @Test
    public void testSetHauteur() {
        Cercle test = new Cercle();
        test.setHauteur(3.14);
        assertEquals("Hauteur : ", 3.14, test.getHauteur(), DELTA);
        assertEquals("Largeur : ", 3.14, test.getLargeur(), DELTA);
    }

    @Test
    public void testSetLargeur() {
        Cercle test = new Cercle();
        test.setLargeur(3.14);
        assertEquals("Hauteur : ", 3.14, test.getHauteur(), DELTA);
        assertEquals("Largeur : ", 3.14, test.getLargeur(), DELTA);
    }

    @Test
    public void testConstructorExceptionNegativeDiameter() {
        expectedException.expect(IllegalArgumentException.class);
        new Cercle(-4);
    }

    @Test
    public void testSetLargeurExceptionNegativeValue() {
        Cercle test = new Cercle();
        expectedException.expect(IllegalArgumentException.class);
        test.setLargeur(-3);
    }

    @Test
    public void testSetHauteurExceptionNegativeValue() {
        Cercle test = new Cercle();
        expectedException.expect(IllegalArgumentException.class);
        test.setHauteur(-3);
    }

    @Test
    public void testContient() {
        Cercle test = new Cercle();
        Coordonnees a = new Coordonnees();
        Coordonnees b = new Coordonnees(7, 4);
        assertTrue("Doit être contenu", test.contient(b));
        assertFalse("Ne doit pas être contenu", test.contient(a));
    }

    @Test
    public void testToString() {
        Cercle test = new Cercle(new Coordonnees(3.14, 3.14), 10);
        String text = "[Cercle] : pos (3,14 , 3,14) dim 10,0 x 10,0 périmètre : 31,42 aire : 78,54 " +
                "couleur = R51,V51,B51";
        assertEquals("Le texte est incorrect", text, test.toString());
        test.setRempli(true);
        text = "[Cercle-Rempli] : pos (3,14 , 3,14) dim 10,0 x 10,0 périmètre : 31,42 aire : 78,54 " +
                "couleur = R51,V51,B51";
        assertEquals("Le texte est incorrect", text, test.toString());
    }

    @Test
    public void testRemplissage() {
        Cercle test = new Cercle();
        assertFalse("La forme n'est pas remplie", test.estRempli());
        test.setRempli(true);
        assertTrue("La forme est remplie", test.estRempli());
    }

    @Test
    public void testGetCouleur() {
        Cercle test = new Cercle();
        assertEquals("Couleur par défaut", UIManager.getColor("Panel.foreground"), test.getCouleur());
    }

    @Test
    public void testSetCouleur() {
        Cercle test = new Cercle();
        test.setCouleur(Color.GREEN);
        assertEquals("Mutateur de la couleur", Color.GREEN, test.getCouleur());
    }
}
