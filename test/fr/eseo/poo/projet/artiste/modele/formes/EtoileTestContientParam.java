package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class EtoileTestContientParam {
    public static final double EPSILON = 1e-3;

    double x, y;
    boolean estContenu;

    public EtoileTestContientParam(double x, double y, boolean estContenu) {
        this.x = x;
        this.y = y;
        this.estContenu = estContenu;
    }

    @Parameters(name = "data[{index}] : ({0}, {1}) => {2}")
    public static Collection<Object[]> data() {
        Object[][] dt = new Object[][]{
                {-1.5, 0, false},
                {-1, 0, true},
                {-0.75, 0, true},
                {-0.5, 0, true},
                {-0.25, 0, true},
                {0, 0, true},
                {1.5, 0, false},
                {1, 0, true},
                {0.75, 0, true},
                {0.5, 0, true},
                {0.25, 0, true},
                {0, -1.5, false},
                {0, -1, true},
                {0, -0.75, true},
                {0, -0.5, true},
                {0, -0.25, true},
                {0, 1.5, false},
                {0, 1, true},
                {0, 0.75, true},
                {0, 0.5, true},
                {0, 0.25, true},
                {0.85, 0.85, false},
                {0.35, 0.35, true},
                {-0.35, 0.35, true},
                {-0.35, -0.35, true},
                {0.35, -0.35, true},
                {0.3, 0.3, true},
                {-0.3, 0.3, true},
                {-0.3, -0.3, true},
                {0.3, -0.3, true},
                {0.4, 0.4, true},
                {-0.4, 0.4, true},
                {-0.4, -0.4, true},
                {0.4, -0.4, true}
        };

        return Arrays.asList(dt);
    }

    @Test
    public void contient() {
        Etoile test = new Etoile(new Coordonnees(-1, -1), 2, 4,
                0, 0.5);
        Coordonnees point = new Coordonnees(this.x, this.y);
        String text = point.toString() + ((this.estContenu) ? " doit être contenu":" ne doit pas être contenu");
        assertEquals(text, this.estContenu, test.contient(point));
    }
}
