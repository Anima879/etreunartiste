package fr.eseo.poo.projet.artiste.modele.formes;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class LigneGetAngleTestParam {

    private static final double DELTA = 1e-3;

    private final double dx, dy, angle;

    public LigneGetAngleTestParam(double dx, double dy, double angle) {
        this.dx = dx;
        this.dy = dy;
        this.angle = angle;
    }

    @Parameters(name = "data[{index}] : ({0}, {1}) => {2}°")
    public static Collection<Object[]> data() {
        Object[][] dt = new Object[][]{
                {10, 10, 45},
                {10, 0, 0},
                {-10, 0, 180},
                {0, 10, 90},
                {-10, 10, 135},
                {0, -10, 270},
                {-10, -10, 225}
        };

        return Arrays.asList(dt);
    }

    @Test
    public void getAngle() {
        Ligne test = new Ligne(dx, dy);
        assertEquals("Angle de " + angle, angle, test.getAngle(), DELTA);
    }
}
