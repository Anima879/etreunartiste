package fr.eseo.poo.projet.artiste.modele.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import javax.swing.UIManager;
import java.awt.Color;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class EtoileTest {

    public static final double EPSILON = 1e-3;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void constructeurTest() {
        Etoile test = new Etoile(new Coordonnees(-1, -1), 2, 4,
                0, 0.5);
        assertEquals("Taille hauteur", 2, test.getLargeur(), EPSILON);
        assertEquals("Taille largeur", 2, test.getHauteur(), EPSILON);
        assertEquals("Rayon", 1, test.getRayon(), EPSILON);
        assertEquals("Nombre branches", 4, test.getNombreBranches());
        assertEquals("Nombre sommets", 8, test.getCoordonnees().size());
        assertEquals("Point 1", new Coordonnees(1, 0), test.getCoordonnees().get(0));
        assertEquals("Point 2", new Coordonnees(0.35, 0.35), test.getCoordonnees().get(1));
        assertEquals("Point 3", new Coordonnees(0, 1), test.getCoordonnees().get(2));
        assertEquals("Point 4", new Coordonnees(-0.35, 0.35), test.getCoordonnees().get(3));
        assertEquals("Point 5", new Coordonnees(-1, 0), test.getCoordonnees().get(4));
        assertEquals("Point 6", new Coordonnees(-0.35, -0.35), test.getCoordonnees().get(5));
        assertEquals("Point 7", new Coordonnees(0, -1), test.getCoordonnees().get(6));
        assertEquals("Point 8", new Coordonnees(0.35, -0.35), test.getCoordonnees().get(7));
        assertEquals("Longueur branche", 0.5, test.getLongueurBranche(), EPSILON);
        assertFalse("N'est pas rempli par défaut", test.estRempli());
    }

    @Test
    public void testConstructorCoord() {
        Etoile test = new Etoile(new Coordonnees(2, 2));
        assertEquals("Position", new Coordonnees(2, 2), test.getPosition());
    }

    @Test
    public void testConstructorTaille() {
        Etoile test = new Etoile(6);
        assertEquals("Position", 6, test.getLargeur(), EPSILON);
        assertEquals("Position", 6, test.getHauteur(), EPSILON);
    }

    @Test
    public void testConstructorExceptionAngle() {
        expectedException.expect(IllegalArgumentException.class);
        new Etoile(new Coordonnees(0, 0), 2, 4,
                10, 0.5);
    }

    @Test
    public void testConstructorExceptionTaille() {
        expectedException.expect(IllegalArgumentException.class);
        new Etoile(new Coordonnees(0, 0), -5, 4,
                0, 0.5);
    }

    @Test
    public void testConstructorExceptionNombreBranche() {
        expectedException.expect(IllegalArgumentException.class);
        new Etoile(new Coordonnees(0, 0), 2, 0,
                0, 0.5);
        new Etoile(new Coordonnees(0, 0), 2, 200,
                0, 0.5);
    }

    @Test
    public void testConstructorExceptionLongueurBranche() {
        expectedException.expect(IllegalArgumentException.class);
        new Etoile(new Coordonnees(0, 0), 2, 4,
                0, -2);
        new Etoile(new Coordonnees(0, 0), 2, 4,
                0, 2);
    }

    @Test
    public void defaultConstructorTest() {
        Etoile test = new Etoile();
        assertEquals("Nombre de branche par défaut", Etoile.NOMBRE_BRANCHES_PAR_DEFAUT, test.getNombreBranches());
        assertEquals("Angle par défaut", Etoile.ANGLE_PREMIERE_BRANCHE_PAR_DEFAUT,
                test.getAnglePremiereBranche(), EPSILON);
        assertEquals("Longueur branche par défaut", Etoile.LONGUEUR_BRANCHE_PAR_DEFAUT,
                test.getLongueurBranche(), EPSILON);
    }

    @Test
    public void constructeurTestDephasage() {
        Etoile test = new Etoile(new Coordonnees(-1, -1), 2, 4,
                Math.PI / 3, 0.5);
        Coordonnees point = new Coordonnees(0.5, 0.87);
        assertEquals("Point 1", point, test.getCoordonnees().get(0));
        assertEquals("Point 2", new Coordonnees(-0.87, 0.5), test.getCoordonnees().get(2));
        assertEquals("Point 3", new Coordonnees(-0.5, -0.87), test.getCoordonnees().get(4));
        assertEquals("Point 4", new Coordonnees(0.87, -0.5), test.getCoordonnees().get(6));
    }

    @Test
    public void aireTest() {
        Etoile test = new Etoile(new Coordonnees(0, 0), 2, 4,
                0, 0.5);
        assertEquals("Aire", 1.4142, test.aire(), EPSILON);
    }

    @Test
    public void perimetreTest() {
        Etoile test = new Etoile(new Coordonnees(0, 0), 2, 4,
                0, 0.5);
        assertEquals("Périmètre", 5.895, test.perimetre(), EPSILON);
    }

    @Test
    public void testContient() {
        Etoile test = new Etoile(new Coordonnees(-1, -1), 2, 4,
                0, 0.5);
        Coordonnees point = new Coordonnees(0, 0.2);
        assertTrue("Est contenu", test.contient(point));
        point = new Coordonnees();
        assertTrue("Est contenu", test.contient(point));
        point = new Coordonnees(1.2, 0);
        assertFalse("N'est pas contenu", test.contient(point));
    }

    @Test
    public void testRemplissage() {
        Etoile test = new Etoile();
        assertFalse("La forme n'est pas remplie", test.estRempli());
        test.setRempli(true);
        assertTrue("La forme est remplie", test.estRempli());
    }

    @Test
    public void testGetCouleur() {
        Etoile test = new Etoile();
        assertEquals("Couleur par défaut", UIManager.getColor("Panel.foreground"), test.getCouleur());
    }

    @Test
    public void testSetCouleur() {
        Etoile test = new Etoile();
        test.setCouleur(Color.GREEN);
        assertEquals("Mutateur de la couleur", Color.GREEN, test.getCouleur());
    }
}
