package fr.eseo.poo.projet.artiste.controleur.actions;

import fr.eseo.poo.projet.artiste.controleur.outils.OutilLigne;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;

public class ActionEffacerTest {
    public ActionEffacerTest() {
        testActionEffacer();
    }

    private void testActionEffacer() {
        JFrame window = new JFrame();
        window.setTitle("Etre un Artiste");
        PanneauDessin drawPanel = new PanneauDessin();
        PanneauBarreOutils toolPanel = new PanneauBarreOutils(drawPanel);
        OutilLigne tool = new OutilLigne();
        drawPanel.associerOutil(tool);
        window.add(drawPanel, BorderLayout.WEST);
        window.add(toolPanel, BorderLayout.EAST);

        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(ActionEffacerTest::new);
    }
}
