package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauBarreOutils;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;

public class OutilEtoileTest {
    OutilEtoileTest() {
        testOutilLigne();
    }

    public void testOutilLigne() {
        JFrame window = new JFrame();
        window.setTitle("Etre un Artiste");

        PanneauDessin pane = new PanneauDessin();
        PanneauBarreOutils tools = new PanneauBarreOutils(pane);
        OutilEtoile tool = new OutilEtoile(tools);
        pane.associerOutil(tool);

        window.add(pane);
        window.add(tools, BorderLayout.EAST);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(OutilEtoileTest::new);
    }
}
