package fr.eseo.poo.projet.artiste.controleur.outils;

import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class OutilLigneTest {

    OutilLigneTest() {
        testOutilLigne();
    }

    public void testOutilLigne() {
        JFrame window = new JFrame();
        window.setTitle("Etre un Artiste");

        PanneauDessin pane = new PanneauDessin();
        OutilLigne tool = new OutilLigne();
        pane.associerOutil(tool);

        window.add(pane);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(OutilLigneTest::new);
    }
}
