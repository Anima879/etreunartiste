package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Cercle;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import java.awt.Color;

public class VueCercleTest {
    public VueCercleTest() {
        testVueCercle();
    }

    private void testVueCercle() {
        JFrame window = new JFrame();
        window.setTitle("Etre un Artiste");

        PanneauDessin canvas = new PanneauDessin();
        // Création des ellipses
        Cercle cercle = new Cercle();
        Cercle cercle2 = new Cercle(new Coordonnees(150, 60), 50);

        // Création des ellipses graphiques
        VueCercle vueCercle = new VueCercle(cercle);
        VueCercle vueCercle2 = new VueCercle(cercle2);

        vueCercle2.getForme().setCouleur(Color.GREEN);

        // Ajout des ellipses graphiques dans le JPanel.
        canvas.ajouterVueForme(vueCercle);
        canvas.ajouterVueForme(vueCercle2);

        window.add(canvas);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(VueCercleTest::new);
    }
}
