package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Ellipse;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import java.awt.Color;

public class VueEllipseTest {
    public VueEllipseTest() {
        testVueEllipse();
    }

    private void testVueEllipse() {
        JFrame window = new JFrame();
        window.setTitle("Etre un Artiste");

        PanneauDessin canvas = new PanneauDessin();
        // Création des ellipses
        Ellipse ellipse = new Ellipse();
        Ellipse ellipse2 = new Ellipse(new Coordonnees(100, 100), 50, 25);

        // Création des ellipses graphiques
        VueEllipse vueEllipse = new VueEllipse(ellipse);
        VueEllipse vueEllipse2 = new VueEllipse(ellipse2);
        vueEllipse2.getForme().setCouleur(Color.CYAN);

        // Ajout des ellipses graphiques dans le JPanel.
        canvas.ajouterVueForme(vueEllipse);
        canvas.ajouterVueForme(vueEllipse2);

        window.add(canvas);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(VueEllipseTest::new);
    }
}
