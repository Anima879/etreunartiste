package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Etoile;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import java.awt.Color;

public class VueEtoileTest {

    public VueEtoileTest() {
        testVueEtoile();
    }

    private void testVueEtoile() {
        JFrame window = new JFrame();
        window.setTitle("Etre un Artiste");

        PanneauDessin canvas = new PanneauDessin();
        // Création des lignes
        Etoile etoile1 = new Etoile();
        Etoile etoile2 = new Etoile(new Coordonnees(200, 100), 100, 9,
                Math.PI / 3, 0.5);
        Etoile etoile3 = new Etoile(new Coordonnees(100, 50), 75, 5,
                -Math.PI / 3, 0.5);

        // Création des lignes graphiques
        VueEtoile vueEtoile1 = new VueEtoile(etoile1);
        VueEtoile vueEtoile2 = new VueEtoile(etoile2);
        VueEtoile vueEtoile3 = new VueEtoile(etoile3);

        vueEtoile2.getForme().setCouleur(Color.BLUE);
        vueEtoile2.getForme().setRempli(true);
        vueEtoile3.getForme().setCouleur(Color.RED);

        // Ajout des lignes graphiques dans le JPanel.
        canvas.ajouterVueForme(vueEtoile1);
        canvas.ajouterVueForme(vueEtoile2);
        canvas.ajouterVueForme(vueEtoile3);

        window.add(canvas);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(VueEtoileTest::new);
    }
}
