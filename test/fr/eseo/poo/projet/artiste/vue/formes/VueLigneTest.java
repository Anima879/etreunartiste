package fr.eseo.poo.projet.artiste.vue.formes;

import fr.eseo.poo.projet.artiste.modele.Coordonnees;
import fr.eseo.poo.projet.artiste.modele.formes.Ligne;
import fr.eseo.poo.projet.artiste.vue.ihm.PanneauDessin;

import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import java.awt.Color;

public class VueLigneTest {

    public VueLigneTest() {
        testVueLigne();
    }

    private void testVueLigne() {
        JFrame window = new JFrame();
        window.setTitle("Etre un Artiste");

        PanneauDessin canvas = new PanneauDessin();
        // Création des lignes
        Ligne ligne1 = new Ligne();
        Ligne ligne2 = new Ligne(new Coordonnees(-50, 0), 200, 140);
        Ligne ligne3 = new Ligne(new Coordonnees(15, 100), 62, 16);

        // Création des lignes graphiques
        VueLigne vueLigne1 = new VueLigne(ligne1);
        VueLigne vueLigne2 = new VueLigne(ligne2);
        VueLigne vueLigne3 = new VueLigne(ligne3);

        vueLigne2.getForme().setCouleur(Color.BLUE);
        vueLigne3.getForme().setCouleur(Color.RED);

        // Ajout des lignes graphiques dans le JPanel.
        canvas.ajouterVueForme(vueLigne1);
        canvas.ajouterVueForme(vueLigne2);
        canvas.ajouterVueForme(vueLigne3);

        window.add(canvas);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(VueLigneTest::new);
    }
}
