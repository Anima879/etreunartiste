package fr.eseo.poo.projet.artiste.vue.ihm;

import javax.swing.SwingUtilities;
import javax.swing.JFrame;
import java.awt.Color;

public class PanneauDessinTest {

    public PanneauDessinTest() {
        testConstructeurParDefaut();
        testConstructeur();
    }

    private void testConstructeurParDefaut() {
        JFrame window = new JFrame();
        window.setTitle("Etre un Artiste");
        window.add(new PanneauDessin());
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        window.pack();
        window.setVisible(true);
    }

    private void testConstructeur() {
        JFrame window = new JFrame();
        window.setTitle("Blues du Businessman");
        window.add(new PanneauDessin(500, 600, Color.blue));
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        window.pack();
        window.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(PanneauDessinTest::new);
    }

}
