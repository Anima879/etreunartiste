package fr.eseo.poo.projet.artiste.vue.ihm;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;

public class PanneauBarreOutilsTest {
    public PanneauBarreOutilsTest() {
        testPanneauBarreOutils();
    }

    private void testPanneauBarreOutils() {
        JFrame window = new JFrame();
        window.setTitle("Etre un Artiste");
        PanneauDessin drawingPanel = new PanneauDessin();
        PanneauBarreOutils toolsPanel = new PanneauBarreOutils(drawingPanel);
        window.add(drawingPanel, BorderLayout.WEST);
        window.add(toolsPanel, BorderLayout.EAST);

        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(PanneauBarreOutilsTest::new);
    }
}
